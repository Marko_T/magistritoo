﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bentchmarking : MonoBehaviour {

	// Use this for initialization
	void Start () {
     //  BenchMarking();
	}

    int i = 0;
    int j = 0;
    float time = 0;

    bool running = false;
    BowyerWatson bw;
	public void BenchMarking()
    {
        for(int i = 0; i < 10; i++)
        {
            int count = 16 * (int)Mathf.Pow(2, i);
            float time = 0;
            for(int j = 0; j < 10; j++)
            {
                BowyerWatson bw = new BowyerWatson(count);
                float ctime = Time.realtimeSinceStartup;
                bw.MakeDiagram();
                time += Time.realtimeSinceStartup - ctime;
            }

            Debug.Log(count + " points took: " + time/10 + "s on average");
        }
    }

    private void Update()
    {
        if (i > 10) return;
        int count = 16 * (int)Mathf.Pow(2, i);

        if (j < 10)
        {
            bw = new BowyerWatson(count);
            float ctime = Time.realtimeSinceStartup;
            bw.MakeDiagram();
            time += Time.realtimeSinceStartup - ctime;
            // StartCoroutine(RunAlgorithm());
        }
        j++;
        if(j == 11)
        {
            i++;
            j = 0;
            Debug.Log(count + " points took: " + time / 10f + "s on average");
            time = 0;
        }
        
    }

    IEnumerator RunAlgorithm()
    {
        running = true;
        yield return null;
        float ctime = Time.realtimeSinceStartup;
        while (bw.DoStep())
        {
            yield return null;
        }
        time += Time.realtimeSinceStartup - ctime;
        Debug.Log("i:" + i + " j:" + j + " time:" + (Time.realtimeSinceStartup - ctime));
        running = false;
        yield return null;
    }
    
}
