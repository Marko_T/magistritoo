﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellDelegate : MonoBehaviour {

    public Vertex vertex;
    public AVoronoiFace face;

    public int index;
    private bool _drawTriangulation = false;

    public void AddPoint(Vector3d loc)
    {
        vertex.AddPoint(loc);
    }

    private void Update()
    {
        if (_drawTriangulation) face.DrawFace(vertex.loc);
    }

    public void DrawTriangulation()
    {
      //  Debug.Log(vertex.planes.Count);
        _drawTriangulation = !_drawTriangulation;
    }
}
