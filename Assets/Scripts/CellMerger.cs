﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class CellMerger {

    private static float edgeTreshold = 0.001f;
    private static float checkTreshold = 0.001f;
    private static float intersectionTreshold = 0.02f;
    public static List<Line> MergeCells(Vertex a, Vertex b)
    {
        List<AVoronoiFace> facesA = a.faces;
        List<AVoronoiFace> facesB = b.faces;

        Dictionary<AVoronoiFace, List<Line>> intersections = new Dictionary<AVoronoiFace, List<Line>>();
        Dictionary<AVoronoiFace, List<Line>> intersections2 = new Dictionary<AVoronoiFace, List<Line>>();

        List<Line> segments = new List<Line>();

        int acount = 0;
        int bcount = 0;
        foreach(AVoronoiFace fromA in facesA) 
        {
            bcount = 0;
            foreach(AVoronoiFace fromB in facesB)
            {
                bool overlaps = FaceFaceIntersection(fromA, a.loc, fromB, b.loc, intersections, intersections2, acount == 4);      
                bcount++;
            }
            acount++;
        }

        for(int i = facesA.Count -1; i>= 0; i--) {
            AVoronoiFace fromA = facesA[i];
          //  fromA.DrawFace(a.loc);
            if (intersections.ContainsKey(fromA))
            {
                AVoronoiFace f = cutFace(fromA, intersections[fromA], intersections2[fromA], a.loc,true);
                f.UpdateObject(); 
            }
            else if(fromA.child != null && b.pointInCell(fromA.child.points[0].loc + a.loc))
            {
                fromA.RemoveObject();
                //   facesA[i] = new InvisibleVoronoiFace(fromA.points, fromA.p, fromA.centerOfMass, a.loc);
            }
        }

        for (int i = facesB.Count - 1; i >= 0; i--)
        {
            AVoronoiFace fromB = facesB[i];
            //  fromB.DrawFace(b.loc);
            if (intersections.ContainsKey(fromB))
            {
                AVoronoiFace f = cutFace(fromB, intersections[fromB], intersections2[fromB], b.loc, false );
                f.child.FindEdgesFromTriagulation();
                // f.UpdateObject();
            }
            else if (fromB.child != null && a.pointInCell(fromB.points[0].loc + b.loc))
            {
                 fromB.RemoveObject();
                //    facesB[i] = new InvisibleVoronoiFace(fromB.points, fromB.p, fromB.centerOfMass, b.loc);
            }

        }
        return segments;
    }

   //TODO lõike joonte otsimisel teha midagi nende väikeste nurkadega
    public static AVoronoiFace cutFace(AVoronoiFace face, List<Line> cutLines, List<Line> cutLines2, Vector3d center, bool normal1 = false,  bool debug = false)
    {

        if (face.child == null) return face;
        //       Debug.Log(cutLines.Count);
        List<Vertex> points = new List<Vertex>();
        List<Vertex> cutLinePoints = new List<Vertex>();
   //     cutLines = cleanCutlines(cutLines, face.child.cutLines);
            foreach(Line g in cutLines)
          // Line g = cutLines[0];
           {
        //    Debug.DrawLine(g.start.vector3, g.end.vector3, Color.red, 100f);
             //  Debug.Log("new");
               if (double.IsInfinity(g.start.x)) continue;

               points.Add(new Vertex(g.start-center));
               points.Add(new Vertex(g.end-center));
               cutLinePoints.Add(new Vertex(g.start - center));
               cutLinePoints.Add(new Vertex(g.end - center));
               Vector3d e = (normal1) ? g.norm1 : g.norm2;
               Vector3d vind = Vector3d.Cross(g.norm1, g.norm2);
               if (normal1)
               {
                   e = Vector3d.Cross(e, vind);
               }
               else
               {
                   e = Vector3d.Cross(vind, e);
               }


               foreach(Vertex v in face.child.points)
               {
                   double s = Vector3d.Dot(-e, v.loc + center - g.start);
            //       if(s > 0)
                   {
                       points.Add(v);
                    }
               }
           }
           
        if (points.Count == 0) return face;
        points = points.Distinct(new VertexComparer()).ToList();
        BowyerWatson2D bw = new BowyerWatson2D(points,face.p,face.centerOfMass);
        bw.MakeDiagram();

        face.child.cutLinePoints.Add(cutLinePoints);
        face.child.cutLines.Add(cutLines);
   /*     face.child.AddCutlines(cutLines, cutLines2);
        face.child.points = bw.points;
        face.child.CleanEdges();
*/

   //     bw.RotatePoints();
  //      LinesTriangleIntersection(bw, face);
      //  bw.InverseRot();

      /*  foreach(List<Line> cutlinelists in face.child.cutLines)
        {
            foreach(Line l in cutlinelists)
            {
                Debug.DrawLine(l.start.vector3, l.end.vector3, Color.red, 10f);
            }
        }*/

        //Needs fixing
 //       face.child.JoinCutlines(cutLines2);
        List<Triangle> cleanTriangulation = bw.triangulation;
        foreach(List<Vertex> previousCutlines in face.child.cutLinePoints)
        {
      //      cleanTriangulation.RemoveAll(x => previousCutlines.Contains(x.p1) && previousCutlines.Contains(x.p2) && previousCutlines.Contains(x.p3));
        }

        foreach(Triangle t in cleanTriangulation)
        {
            foreach(Edge<Triangle> e in t.edges)
            {
                if(e.left == t && e.right != null && !cleanTriangulation.Contains(e.right))
                {
                    e.right = null;
                }
                if (e.right == t && e.left != null && !cleanTriangulation.Contains(e.left))
                {
                    e.left = null;
                }
            }
        }
    //    cleanTriangulation.RemoveAll(x => cutLinePoints.Contains(x.p1) && cutLinePoints.Contains(x.p2) && cutLinePoints.Contains(x.p3));
    //    face.child.setTriangulation(cleanTriangulation(bw.triangulation, face.child.Edges, face.loc, debug));
        face.child.setTriangulation(cleanTriangulation);
        face.child.points = bw.points;
     //   face.child.CleanEdges();
        return face;
    }

    public static List<Line> cleanCutlines(List<Line> cutlines, List<List<Line>> cutlinesList)
    {
        List<List<IndexLine>> clean = new List<List<IndexLine>>();
        List<List<Vector3d>> overlaps = new List<List<Vector3d>>();
        int groupId = 0;

        foreach (List<Line> lines in cutlinesList)
        {
            clean.Add(new List<IndexLine>());
            foreach (Line l in lines)
            {
                bool found = false;
                IndexLine cleanLine = new IndexLine();
                for (int i = 0; i < overlaps.Count; i++)
                {
                    if (overlaps[i].FindIndex(x => (x - l.start).sqrMagnitude < 0.001) != -1)
                    {
                        overlaps[i].Add(l.start);
                        found = true;
                        cleanLine.start = i;
                    }
                }

                if (!found)
                {
                    overlaps.Add(new List<Vector3d>());
                    overlaps[overlaps.Count - 1].Add(l.start);
                    cleanLine.start = groupId;
                    groupId++;
                }

                found = false;
                for (int i = 0; i < overlaps.Count; i++)
                {
                    if (overlaps[i].FindIndex(x => (x - l.end).sqrMagnitude < 0.001) != -1)
                    {
                        overlaps[i].Add(l.end);
                        found = true;
                        cleanLine.end = i;
                    }
                }

                if (!found)
                {
                    overlaps.Add(new List<Vector3d>());
                    overlaps[overlaps.Count - 1].Add(l.end);
                    cleanLine.end = groupId;
                    groupId++;
                }
                clean[clean.Count - 1].Add(cleanLine);
            }
        }
        clean.Add(new List<IndexLine>());
        foreach (Line l in cutlines)
        {
            bool found = false;
            IndexLine cleanLine = new IndexLine();
            for (int i = 0; i < overlaps.Count; i++)
            {
                if (overlaps[i].FindIndex(x => (x - l.start).sqrMagnitude < 0.001) != -1)
                {
                    overlaps[i].Add(l.start);
                    found = true;
                    cleanLine.start = i;
                }
            }

            if (!found)
            {
                overlaps.Add(new List<Vector3d>());
                overlaps[overlaps.Count - 1].Add(l.start);
                cleanLine.start = groupId;
                groupId++;
            }

            found = false;
            for (int i = 0; i < overlaps.Count; i++)
            {
                if (overlaps[i].FindIndex(x => (x - l.end).sqrMagnitude < 0.001) != -1)
                {
                    overlaps[i].Add(l.end);
                    found = true;
                    cleanLine.end = i;
                }
            }

            if (!found)
            {
                overlaps.Add(new List<Vector3d>());
                overlaps[overlaps.Count - 1].Add(l.end);
                cleanLine.end = groupId;
                groupId++;
            }
            clean[clean.Count -1].Add(cleanLine);
        }


       for(int i = 0; i < cutlinesList.Count; i++)
        {
            for(int j = 0; j < cutlinesList[i].Count; j++)
            {
                Line l = cutlinesList[i][j];
                IndexLine il = clean[i][j];
                l.start = overlaps[il.start][0];
                l.end = overlaps[il.end][0];
            }
        }

       for(int i = 0; i < cutlines.Count; i++)
        {
            Line l = cutlines[i];
            IndexLine il = clean[clean.Count -1][i];
            l.start = overlaps[il.start][0];
            l.end = overlaps[il.end][0];
        }
        /*
        for(int i = 0; i < cutlines.Count; i++)
        {
            Line a = cutlines[i];          
            for(int j = i+1; j < cutlines.Count; j++)
            {
                Line b = cutlines[j];

                if((a.start-b.start).sqrMagnitude < 0.001)
                {
                    cutlines[i].start = b.start;
                }else if((a.start - b.end).sqrMagnitude < 0.001)
                {
                    cutlines[i].start = b.end;
                }
                else if ((a.end - b.start).sqrMagnitude < 0.001)
                {
                    cutlines[i].end = b.start;
                }
                else if ((a.end - b.end).sqrMagnitude < 0.001)
                {
                    cutlines[i].end = b.end;
                }              
            }
        }
        */

        return cutlines;
    }

    public static List<Triangle> cleanTriangulation(List<Triangle> triangulation, List<Edge<Triangle>> edges, Vector3d loc, bool debug = false)
    {
        if (triangulation.Count == 0) return triangulation;
        if (debug)
        {
            foreach (Edge<Triangle> e in edges)
            {
                Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.cyan, 10f);
            }
        }
        bool keep = true;
        List<Triangle> queue = new List<Triangle>();
        queue.Add(triangulation[0]);
        triangulation[0].keep = keep;
        while(queue.Count > 0)
        {
            Triangle tris = queue[0];
            queue.RemoveAt(0);
            foreach(Edge<Triangle> e in tris.edges)
            {
                if (debug)
                {
                    if (edges.IndexOf(e) != -1)
                    {
                        Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.blue, 10f);
                    }
                }
                if(e.left == tris && e.right != null)
                {
                    if (!e.right.processed)
                    {
                        // Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.blue, 10f);
                        if (edges.IndexOf(e) != -1)
                        {
                       // Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.blue, 10f);
                        e.right.keep = !tris.keep;
                        }
                        else
                        {
                            e.right.keep = tris.keep;
                        }
                        e.right.processed = true;
                        queue.Add(e.right);
                    }
                }
                else if (e.right == tris && e.left != null)
                {
                    if (!e.left.processed)
                    {
                        //    Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.blue, 10f);
                        if (edges.IndexOf(e) != -1)
                        {
                      //  Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.blue, 10f);
                        e.left.keep = !tris.keep;
                        }
                        else
                        {
                            e.left.keep = tris.keep;
                        }
                        e.left.processed = true;
                        queue.Add(e.left);
                    }
                }else if(e.left == tris && e.right == null)
                {
                    if (edges.IndexOf(e) != -1)
                    {
                        keep = tris.keep;
                    }
                }
                else if (e.right == tris && e.left == null)
                {
                    if (edges.IndexOf(e) != -1)
                    {
                        keep = tris.keep;
                    }else
                    {
                    //     Debug.DrawLine(e.start.loc.vector3, e.end.loc.vector3, Color.blue, 10f);
                    }
                }
            }  
        }
        
        for(int i = triangulation.Count-1; i >= 0; i--)
        {
            if(triangulation[i].keep != keep)
            {
                triangulation.RemoveAt(i);
            }
        }
        return triangulation;
    }

    public static void LinesTriangleIntersection(BowyerWatson2D bw, AVoronoiFace face)
    {
        Quaternion rot = face.rotationToXYPlane;
        Quaternion inverse = face.inverseRotationToXYPLane;

        for(int i = 0; i < face.child.cutLines.Count; i++)
        {
            List<Line> cutLines = face.child.cutLines[i];
            List<Vertex> cutLinePoints = face.child.cutLinePoints[i];
            List<Triangle> triangulation = bw.triangulation.ToList();
            for(int j = cutLines.Count -1; j >= 0; j--)
            {
                Line temp = new Line(new Vector3d(rot * (cutLines[j].start - face.loc).vector3), new Vector3d(rot * (cutLines[j].end - face.loc).vector3), new Vector3d(rot * cutLines[j].dir.vector3));
                foreach (Triangle t in triangulation)
                {
                    List<Vector3d> intersections = new List<Vector3d>();
                    foreach (Edge<Triangle> e in t.edges)
                    {
                        if (RayPointDistance(temp, e.start.loc) < edgeTreshold && RayPointDistance(temp, e.end.loc) < edgeTreshold)
                        {
                            continue;
                        }
                        Vector3d intersection = LineLineIntersection(temp, e.start.loc, e.end.loc);
                        if (double.IsInfinity(intersection.x)) continue;
                        if (!PointOnSegment(temp.start, temp.end, intersection)) continue;
                  //      if(face.child.cutLines.Any(x => face.PointInConvex(x, new Vector3d(inverse*intersection.vector3)+ face.loc)))continue;
                        if (bw.AddPoint(intersection))
                        {
                            intersections.Add(intersection);
                        }
                    }
                    if (intersections.Count > 0)
                    {
                        intersections.Add(temp.start);
                        intersections.Add(temp.end);

                        intersections = intersections.OrderBy(x => x.x).ThenBy(x => x.y).ToList();
                        for (int k = 0; k < intersections.Count - 1; k++)
                        {
                            Vector3d start = new Vector3d(inverse * intersections[k].vector3) + face.loc;
                            Vector3d end = new Vector3d(inverse * intersections[k+1].vector3) + face.loc;
                            Line newline = new Line(start, end, end-start);
                            newline.norm1 = cutLines[j].norm1;
                            newline.norm2 = cutLines[j].norm2;
                            cutLines.Add(newline);
                            cutLinePoints.Add(new Vertex(start - face.loc));
                            cutLinePoints.Add(new Vertex(end - face.loc));
                        }
                        cutLines.RemoveAt(j);
                    }
                }
            }
        }
    }

    //Make into 2D calculation
   /* public static List<Vector3d> LinesTriangleIntersection(List<Line> lines, List<Triangle> triangles, AVoronoiFace face)
    {
        List<Vector3d> points = new List<Vector3d>();

        Quaternion rot = face.rotationToXYPlane;
        Quaternion inverse = face.inverseRotationToXYPLane;

        List<Line> remove = new List<Line>();
        List<Line> newLines = new List<Line>();
        foreach (Line g in lines)
        {

            Line temp = new Line(new Vector3d(rot * (g.start - face.loc).vector3), new Vector3d(rot * (g.end - face.loc).vector3), new Vector3d(rot * g.dir.vector3));
            foreach (Triangle t in triangles)
            {
                List<Vector3d> intersections = new List<Vector3d>();
                foreach (Edge<Triangle> e in t.edges)
                {
                    //  Debug.DrawLine(e.start + center, e.end + center, Color.red, 10f);
                       if (RayPointDistance(temp, e.start.loc) < edgeTreshold && RayPointDistance(temp, e.end.loc) < edgeTreshold)
                        {
                            continue;
                        }
                    Vector3d edgeStart = e.start.loc ;
                    Vector3d edgeEnd =(e.end.loc);
                    Vector3d intersection = LineLineIntersection(temp, edgeStart, edgeEnd);
                    //Vector3d intersection = LineLineIntersection(g, e, center);
                    if (double.IsInfinity(intersection.x)) continue;
                    if (!PointOnSegment(temp.start, temp.end, intersection)) continue;
                 
                    int index = intersections.FindIndex(v => (v - intersection).sqrMagnitude < 0.001);
                    if (index == -1)
                    {
                        intersections.Add(intersection);
                    }
                }
                if (intersections.Count > 0)
                {
               
                    points.AddRange(intersections);
                }
            }
        }
        return points.Distinct(new VectorComparer()).ToList();
    }
    */
    public static bool PointOnSegment(Vector3d start, Vector3d end, Vector3d point)
    {
        return Mathd.Min(end.x, start.x) <= point.x && point.x <= Mathd.Max(end.x, start.x) &&
            Mathd.Min(end.y, start.y) <= point.y && point.y <= Mathd.Max(end.y, start.y) &&
            (point - start).sqrMagnitude > 0.001 && (point - end).sqrMagnitude > 0.001;
    }


    private static bool FaceFaceIntersection(AVoronoiFace a, Vector3d aloc, AVoronoiFace b, Vector3d bloc, Dictionary<AVoronoiFace, List<Line>> intersections,
        Dictionary<AVoronoiFace, List<Line>>  intersections2, bool debug = false)
    {
        debug = true;
        bool overlapsb = false;
      //  if (a.Equals(b)) return new List<Line>();
        Vector3d u = Vector3d.Cross(a.normal, b.normal).normalized;
        if (u == Vector3d.zero) return false;
        Vector3d intersection = LinePlaneIntersection(new Line(a.centerOfMass + aloc, Vector3d.Cross(a.normal, u)), b, bloc);
        if (a.child != null)
        {
            List<Line> intersectionsAChild = LinePolygonIntersection(new Line(intersection, u), a.child, aloc, Color.red);
            List<Line> intersectionsB = LinePolygonIntersection(new Line(intersection, u), b, bloc, Color.blue);

            List<Line> overlaps = LineSegmentOverlaps(intersectionsAChild, a.normal, intersectionsB, b.normal, a.rotationToXYPlane, a.inverseRotationToXYPLane);

            if (debug)
            {
                foreach (Line l in intersectionsAChild)
                {
             //       Debug.DrawLine(l.start.vector3, l.end.vector3, Color.green, 10f);
                }
                foreach (Line l in intersectionsB)
                {
            //           Debug.DrawLine(l.start.vector3, l.end.vector3, Color.blue, 10f);
                }
                foreach (Line l in overlaps)
                {
            //        Debug.DrawLine(l.start.vector3, l.end.vector3, Color.red, 100f);
                }
            }
            if (overlaps.Count != 0)
            {
                if (!intersections.ContainsKey(a)) intersections.Add(a, new List<Line>());
                intersections[a].AddRange(overlaps);
                overlapsb = true;
            }
            if (!intersections2.ContainsKey(a)) intersections2.Add(a, new List<Line>());
            intersections2[a].AddRange(intersectionsB);
        }

        if(b.child != null)
        {
            List<Line> intersectionsBChild = LinePolygonIntersection(new Line(intersection, u), b.child, bloc, Color.blue);
            List<Line> intersectionsA = LinePolygonIntersection(new Line(intersection, u), a, aloc, Color.red);
            List<Line> overlaps = LineSegmentOverlaps(intersectionsA, a.normal, intersectionsBChild, b.normal, b.rotationToXYPlane, b.inverseRotationToXYPLane);

            if (debug)
            {
                foreach (Line l in intersectionsA)
                {
                //       Debug.DrawLine(l.start.vector3, l.end.vector3, Color.blue, 10f);
                }
                foreach (Line l in intersectionsBChild)
                {
                 //      Debug.DrawLine(l.start.vector3, l.end.vector3, Color.green, 10f);
                }

                foreach (Line l in overlaps)
                {
                  //     Debug.DrawLine(l.start.vector3, l.end.vector3, Color.red, 10f);
                }
            }
            if (overlaps.Count != 0)
            {
                if (!intersections.ContainsKey(b)) intersections.Add(b, new List<Line>());
                intersections[b].AddRange(overlaps);
                overlapsb = true;
            }
            if (!intersections2.ContainsKey(b)) intersections2.Add(b, new List<Line>());
            intersections2[b].AddRange(intersectionsA);
        }
       
        return overlapsb;
    }

    private static Vector3d rotate(Vector3d point, Quaternion rotation)
    {
        return new Vector3d(rotation * point.vector3);
    }

    private static Vector3d LinePlaneIntersection(Line a, AVoronoiFace b, Vector3d bloc)
    {
        Vector3d w = (b.centerOfMass + bloc) - a.start;
        double s = Vector3d.Dot(b.normal, w) / Vector3d.Dot(b.normal, a.dir);
        return a.start + s*a.dir;
    }

    private static List<Line> LinePolygonIntersection(Line a, AVoronoiFace b, Vector3d bloc, Color color, bool debug = false)
    {
        if (b == null) return new List<Line>();
        List<Line> lines = new List<Line>();
        List<Vector3d> intersections = new List<Vector3d>();
       
        Quaternion rot = b.rotationToXYPlane;
        Quaternion inverse = b.inverseRotationToXYPLane;
        Line temp = new Line(new Vector3d(rot * a.start.vector3), new Vector3d(rot* a.dir.vector3));
        if (debug)
        {
            Debug.DrawRay(a.start.vector3, a.dir.vector3, color, 10f);
        }
        foreach (Edge<Triangle> edge in b.Edges)
        {

            Vector3d edgeStart = new Vector3d(rot * (edge.start.loc + bloc).vector3);
            Vector3d edgeEnd = new Vector3d(rot * (edge.end.loc + bloc).vector3);

            if(Vector3d.Cross(temp.dir, edgeEnd - edgeStart).sqrMagnitude < 0.001 && RayPointDistance(temp, edgeStart) < 0.001 &&
                RayPointDistance(temp, edgeEnd) < 0.001)
            {
                lines.Add(new Line(rotate(edgeStart, inverse), rotate(edgeEnd, inverse), rotate(edgeEnd-edgeStart, inverse)));
                continue;
            }
            Vector3d intersection = LineLineIntersection(temp, edgeStart, edgeEnd);

            if (double.IsInfinity(intersection.x)) continue;

            int index = intersections.FindIndex(v => (v - intersection).sqrMagnitude < 0.0001f);
            if(index == -1)
            {
                if(debug) Debug.DrawRay(intersection.vector3, Vector3.up, Color.magenta, 10f);
                intersections.Add(intersection);
            }
        }
       
        intersections = intersections.OrderBy(pt => pt.x).ThenBy(pt => pt.y).ThenBy(pt => pt.z).ToList();
      //  intersections.Sort((x, y) => CompareVectors(x, y));
       
        for (int i = 0; i < intersections.Count-1; i += 2)
        {
            lines.Add(new Line(rotate(intersections[i],inverse), rotate(intersections[i + 1], inverse), rotate(intersections[i + 1] - intersections[i], inverse)));
        }
       
        return lines;
    }

    private static int CompareVectors(Vector3d a, Vector3d b)
    {
        if((a-b).sqrMagnitude < 0.001)
        {
            return 0;
        }
        Debug.Log(b.x > a.x);
        Debug.Log(b.y < a.y);
        Debug.Log(b.z < a.z);
        if (b.x > a.x)
        {
            Debug.Log("bx > ax");
            return -1;
        }
        if(b.x < a.x)
        {
            Debug.Log("bx < ax");
            return 1;
        }
        if (b.y > a.y)
        {
            Debug.Log("by > ay");
            return -1;
        }
        if (b.y < a.y)
        {
            Debug.Log("by < ay");
            return 1;
        }
        if (b.z > a.z)
        {
            Debug.Log("bz > az");
            return -1;
        }
        if (b.z < a.z)
        {
            Debug.Log("bz < az");
            return 1;
        }
        Debug.Log("bx = ax");
        return 0;
    }

    private static List<Line> LineSegmentOverlaps(List<Line> segmentsA, Vector3d planeNorm1, List<Line> segmentsB, Vector3d planeNorm2, Quaternion rot, Quaternion inv, bool debug = false)
    {
        if (segmentsA.Count == 0 || segmentsB.Count == 0) return new List<Line>();
        List<Line> overlaps = new List<Line>();

        int acount = 0;
        int bcount = 0;
        foreach(Line a in segmentsA)
        {
            Line temp = new Line(rotate(a.start, rot), rotate(a.end, rot), rotate(a.dir, rot));
            bcount = 0;
            foreach(Line b in segmentsB)
            {
                Line temp2 = new Line(rotate(b.start, rot), rotate(b.end, rot), rotate(b.dir, rot));
                if (debug)
                {
               //     Debug.DrawLine(temp.start.vector3, temp.end.vector3, Color.green, 10f);
               //     Debug.DrawLine(temp2.start.vector3, temp2.end.vector3, Color.blue, 10f);
                }

                Line overlap = temp.OverLaps(temp2);
                if (overlap.isInfinity())
                {
                    overlap = temp2.OverLaps(temp);
                }
                if (!overlap.isInfinity())
                {
                    Line newOverlap = new Line(rotate(overlap.start, inv), rotate(overlap.end, inv), rotate(overlap.dir, inv));
                    newOverlap.norm1 = planeNorm1;
                    newOverlap.norm2 = planeNorm2;
                    overlaps.Add(newOverlap);
                    int index = GameManager.cutLinePoints.FindIndex(x => (newOverlap.start - x).sqrMagnitude < 0.01);
                    if(index != -1)
                    {
                        newOverlap.start = GameManager.cutLinePoints[index];
                    }else
                    {
                        GameManager.cutLinePoints.Add(newOverlap.start);
                    }
                    index = GameManager.cutLinePoints.FindIndex(x => (newOverlap.end - x).sqrMagnitude < 0.01);
                    if (index != -1)
                    {
                        newOverlap.end = GameManager.cutLinePoints[index];
                    }else
                    {
                        GameManager.cutLinePoints.Add(newOverlap.end);
                    }
                    bcount++;
                }
                acount++;
            }
           
        }
          return overlaps;

    }

    private static double RayPointDistance(Line a, Vector3d point)
    {
        return Vector3d.Cross(a.dir.normalized, point - a.start).magnitude;
    }

    public static Vector3d LineLineIntersection(Line a, Vector3d edgeStart, Vector3d edgeEnd)
    {
        /*
         * a.start + s*a.dir = intersection
         * edgeSTart + t*(edgeEnd - endgeStart) = intersection
         * a.start + s*a.dir = edgeSTart + t*(edgeEnd - endgeStart) |X a.dir
         * a.start X a.dir = edgeStart X a.dir + t* (edgedIr) X a.dir
         * (a.start - edge.start) X a.dir  / edgedir X a.dir = t
         */

        double t = Cross(a.start - edgeStart, a.dir) / Cross(edgeEnd - edgeStart, a.dir);
        if (t < 0 || t > 1) return Vector3d.positiveInfinity;
        return edgeStart + t * (edgeEnd - edgeStart);
    }

    public static double Cross(Vector3d a, Vector3d b)
    {
        return a.x * b.y - a.y * b.x;
    }

    public static Vector3d LineLineIntersection(Line a, Edge<Triangle> b, Vector3d bloc)
    {

        /*
         * a.start + s*a.dir = intersection
         * a.start - b.start = w
         *  intersection = w + s*a.dir;
         *  Dot(b.norm, w+s*a.dir) = 0
         * Dot(b.norm , w) + s* Dot(b.norm*adir) = 0
         *  s = -DOt(b.norm,w)/Dot(b.norm*a.dir)
         */

        /*
         * u = adir
         * v = e dir
         */

        double ad = Vector3d.Dot(a.dir.normalized, a.dir.normalized);
        double bd = Vector3d.Dot(b.end - b.start, a.dir.normalized);
        double c = Vector3d.Dot(b.end - b.start, b.end - b.start);
        double d = Vector3d.Dot(a.dir.normalized, a.start - b.start.loc - bloc);
        double e = Vector3d.Dot(b.end - b.start, a.start - b.start.loc - bloc);

        double tc = (ad * e - bd * d) / (ad * c - bd * bd);

        if (tc < -0.001 || tc > 1.001) return Vector3d.positiveInfinity;
        return b.start + tc * (b.end - b.start) + bloc;
  /*
        Vector3 A = a.dir;
        Vector3 B = b.end - b.start;
        Vector3 C = (b.start + bloc) - a.start;

        float s = Vector3.Dot(Vector3.Cross(C, B), Vector3.Cross(A, B)) / Vector3.Cross(A, B).sqrMagnitude;
        return a.start + A * s;*/
    }

    public static Vector3d LineLineIntersection(Line a, Line b)
    {
        Vector3d n = Vector3d.Cross(a.end - a.start, b.end - b.start);
        if (Math.Abs(Vector3d.Dot(n, a.start) - Vector3d.Dot(n, b.start)) > 0.01) return Vector3d.positiveInfinity;
        Vector3d np = Vector3d.Cross(a.end - a.start, n);
        double c = Vector3d.Dot(np, a.start);
        double t = (c - Vector3d.Dot(np, b.start)) / Vector3d.Dot(np, b.end - b.start);
        if (t < -0.001 || t > 1.001) return Vector3d.positiveInfinity;

        np = Vector3d.Cross(n, b.end - b.start);
        c = Vector3d.Dot(np, b.start);
        t = (c - Vector3d.Dot(np, a.start)) / Vector3d.Dot(np, a.end - a.start);

        if (t < -0.001 || t > 1.001) return Vector3d.positiveInfinity;
        return a.start + (a.end - a.start) * t;
    }

    public static Vector3d LineLineIntersection(Edge<Triangle> a, Edge<Triangle> b)
    {
        Vector3d n = Vector3d.Cross(a.end - a.start, b.end - b.start);
        if (Math.Abs(Vector3d.Dot(n, a.start.loc) - Vector3d.Dot(n, b.start.loc)) > 0.01) return Vector3d.positiveInfinity;
        Vector3d np = Vector3d.Cross(a.end - a.start, n);
        double c = Vector3d.Dot(np, a.start.loc);
        double t = (c - Vector3d.Dot(np, b.start.loc)) / Vector3d.Dot(np, b.end - b.start);
        if (t < -0.001f || t > 1.001f) return Vector3d.positiveInfinity;

        np = Vector3d.Cross(n, b.end - b.start);
        c = Vector3d.Dot(np, b.start.loc);
        t = (c - Vector3d.Dot(np, a.start.loc)) / Vector3d.Dot(np, a.end - a.start);

        if (t < -0.001f || t > 1.001f) return Vector3d.positiveInfinity;
        return a.start + (a.end - a.start) * t;
    }
}

public class Line
{
    public Vector3d dir;
    public Vector3d start;
    public Vector3d end;

    public Vector3d norm1;
    public Vector3d norm2;

    public static Line NoLine = new Line(Vector3d.positiveInfinity, Vector3d.positiveInfinity, Vector3d.positiveInfinity);

    public List<Line> overlaps = new List<Line>();

    public Line(Vector3d start, Vector3d dir)
    {
        this.dir = dir;
        this.start = start;
    }

    public Line(Vector3d start, Vector3d end, Vector3d dir)
    {
        this.start = start;
        this.end = end;
        this.dir = dir;
    }

    public Line OverLaps(Line other)
    {
        double slope = (end.y - start.y) / (end.x - start.x);
        bool isHorizontal = Mathd.Abs(slope) < 0.01;
        bool isDecending = slope > 0 && !isHorizontal;
        double invertY = !isDecending ? -1: 1;

        Vector3d min1 = new Vector3d(Mathd.Min(start.x, end.x), Mathd.Min(start.y * invertY, end.y * invertY));
        Vector3d max1 = new Vector3d(Mathd.Max(start.x, end.x), Mathd.Max(start.y * invertY, end.y* invertY));

        Vector3d min2 = new Vector3d(Mathd.Min(other.start.x, other.end.x), Mathd.Min(other.start.y * invertY, other.end.y * invertY));
        Vector3d max2 = new Vector3d(Mathd.Max(other.start.x, other.end.x), Mathd.Max(other.start.y * invertY, other.end.y * invertY));

        Vector3d minIntersection = new Vector3d(Mathd.Max(min1.x, min2.x), Mathd.Max(min1.y, min2.y), start.z);
        
        Vector3d MaxIntersection = new Vector3d(Mathd.Min(max1.x, max2.x), Mathd.Min(max1.y, max2.y), start.z);
        

        bool intersect = minIntersection.x < MaxIntersection.x && minIntersection.y < MaxIntersection.y;
        if (intersect)
        {
            minIntersection.y *= invertY;
            MaxIntersection.y *= invertY;
            return new Line(minIntersection, MaxIntersection, MaxIntersection - minIntersection);
        }
        else
        {
            return NoLine;
        }
        if (CompareVectors(start, other.start) == 1) return NoLine;
        //Leida migni hea loogika selle leidmisek

        if ((dir.normalized - other.dir.normalized).sqrMagnitude > 0.01f)
        {
            Line temp = new Line(end, start, -dir);
            if ((temp.dir.normalized - other.dir.normalized).sqrMagnitude > 0.01f) return NoLine;
            if (CompareVectors(temp.end, other.start) <= 0) return NoLine;
            if (CompareVectors(other.end, temp.start) <= 0) return NoLine;
            if (CompareVectors(temp.end, other.end) == -1)
            {
                return new Line(other.start, temp.end, temp.end - other.start);
            }
            else
            {
                return other;
            }
        }
        else
        {
            if (CompareVectors(end, other.start) <= 0) return NoLine;
            if (CompareVectors(other.end, start) <= 0) return NoLine;
            if (CompareVectors(end, other.end) == -1)
            {
                return new Line(other.start, end,end -  other.start);
            }
            else
            {
                return other;
            }
        }
    }

    private static int CompareVectors(Vector3d a, Vector3d b)
    {
        if ((a - b).sqrMagnitude < 0.001)
        {
            return 0;
        }
  
        if (b.x > a.x)
        {
          
            return -1;
        }
        if (b.x < a.x)
        {
           
            return 1;
        }
        if (b.y > a.y)
        {
           
            return -1;
        }
        if (b.y < a.y)
        {
          
            return 1;
        }
        if (b.z > a.z)
        {
         
            return -1;
        }
        if (b.z < a.z)
        {
         
            return 1;
        }
     
        return 0;
    }

    public void AddOverlap(List<Line> processed)
    {
        bool found = false;
        foreach (Line l in processed) {
            if (this.Equals(l))
            {
                l.overlaps.Add(this);
                found = true;
                break;
            }
        }
        if(!found)
        {
            processed.Add(this);
        }
    }

    public bool isInfinity()
    {
        return double.IsInfinity(start.x) && double.IsInfinity(end.x) && double.IsInfinity(dir.x);
    }

    public bool Equals(Line other)
    {
        return (this.start - other.start).sqrMagnitude < 0.001 && (this.end - other.end).sqrMagnitude < 0.001 ||
            (this.end - other.start).sqrMagnitude < 0.001 && (this.start - other.end).sqrMagnitude < 0.001;
    }

    public override string ToString()
    {
        return "( start: " + start + " end: " + end + " dir: " + dir + ")";
    }
}

public struct IndexLine
{
    public int start;
    public int end;
}

class VertexComparer : IEqualityComparer<Vertex>
{
    public bool Equals(Vertex x, Vertex y)
    {
        return (x-y).sqrMagnitude < 0.001;
    }

    public int GetHashCode(Vertex obj)
    {
        return (int)obj.loc.sqrMagnitude;
    }
}


class VectorComparer : IEqualityComparer<Vector3d>
{
    public bool Equals(Vector3d x, Vector3d y)
    {
        return (x - y).sqrMagnitude < 0.001;
    }

    public int GetHashCode(Vector3d obj)
    {
        return (int)obj.sqrMagnitude;
    }
}