﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;

public class BowyerWatson{

    public Tetrahedron super_tetrahedron;
    public List<Vertex> points = new List<Vertex>();
    public List<Vertex> controlPoints = new List<Vertex>();
    public  List<Vertex> vertices = new List<Vertex>();

    private List<Tetrahedron> badTerahedrons = new List<Tetrahedron>();
    private List<Tetrahedron> queue = new List<Tetrahedron>();
    private List<Tetrahedron> checke = new List<Tetrahedron>();

    int i = 0;
    bool done = false;

    public List<Tetrahedron> triangulation;
    private Octree octree;

    public Vertex AddPoint(Vector3d point)
    {
        points.Add(new Vertex(point));
        DoStep();

        return vertices.Find(c => c.loc == point);
    }

    public List<Vertex> AddPoints(List<Vector3d> points)
    {
        Debug.Log(points.Count);
        List<Vertex> newCells = new List<Vertex>();
        Vector3d shift = Vector3d.zero;
        if (GameManager.materialized.Count > 1)
        {
            Vertex closest = this.points.OrderBy(x => (x - (points[0])).sqrMagnitude).ToList()[0];
            List<Tetrahedron> neighbours = closest.neighbors;
            foreach (Vertex v in GameManager.materialized.Skip(1))
            {
                List<Tetrahedron> commonNeighbours = neighbours.Intersect(v.neighbors).ToList();
                bool found = false;
                foreach(Tetrahedron t in commonNeighbours)
                {
                    if (t.withinRange(points[0]))
                    {
                        newCells.Add(closest);
                        shift = closest - points[0];
                        points.RemoveAt(0);
                        found = true;
                        break;
                    }
                }
                if (found) break;
            }

            points.RemoveAll(x => GameManager.materialized.Skip(1).Any(v => v.pointInCell(x+shift)));
            for (int i = points.Count - 1; i >= 0; i--)
            {
                 closest = this.points.OrderBy(x => (x - (points[i] + shift)).sqrMagnitude).ToList()[0];
                 neighbours = closest.neighbors;
                foreach (Vertex v in GameManager.materialized.Skip(1))
                {
                    List<Tetrahedron> commonNeighbours = neighbours.Intersect(v.neighbors).ToList();
                    bool found = false;
                    foreach (Tetrahedron t in commonNeighbours)
                    {
                        if (t.withinRange(points[i]+shift))
                        {
                            if(newCells.FindIndex(x=> (x-closest).sqrMagnitude < 0.001) == -1)newCells.Add(closest);
                            points.RemoveAt(i);
                            found = true;
                            break;
                        }
                    }
                    if (found) break;
                }
            }
        }


        Debug.Log(points.Count);
        foreach(Vector3d v in points)
        {
            Vertex temp = new Vertex(v + shift);
            newCells.Add(temp);
            this.points.Add(temp);
            DoStep();
        }

  //      newCells = newCells.Distinct(new VertexComparer()).ToList();
        return newCells;
    }

    public BowyerWatson(int count)
    {

        generatePoint(count);
        createSuperTetrahedron();


        triangulation = new List<Tetrahedron>();
        triangulation.Add(super_tetrahedron);
    }

    public BowyerWatson(List<Vertex> points)
    {
        this.points = points;
        createSuperTetrahedron();

        triangulation = new List<Tetrahedron>();
        triangulation.Add(super_tetrahedron);
    }

    public BowyerWatson(Vector3d center, int radius)
    {
        createSuperTetrahedron(center, radius);
    }

    public void generatePoint(int count)
    {
            octree = new Octree(new Vector3d(0, 150, 0 ), new Vector3d(401, 251, 401));
            for (int i = 0; i < count; i++)
            {
                points.Add(new Vertex(Random.Range(-350, 350), Random.Range(-50, 300), Random.Range(-350, 350)));
            }

            int controlCOunt = 20;
            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(Random.Range(-400, 400), -100, Random.Range(-400, 400)));
            }
            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(Random.Range(-400, 400), 400, Random.Range(-400, 400)));
            }


            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(Random.Range(-400, 400), Random.Range(-100, 400), -400 ));
            }

            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(Random.Range(-400, 400), Random.Range(-100, 400), 400));
            }

            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(-400, Random.Range(-100, 400), Random.Range(-400, 400)));
            }

            for (int i = 0; i < controlCOunt; i++)
            {
                controlPoints.Add(new Vertex(400, Random.Range(-100, 400), Random.Range(-400, 400)));
            }

            points.AddRange(controlPoints);
        
    }

    private void createSuperTetrahedron(Vector3d center, int radius)
    {
        Vertex p1 = new Vertex(center + new Vector3d(0, 3 * radius, 0));
        Vertex p2 = new Vertex(center + new Vector3d(-radius * Mathd.Sqrt(2.5), -radius, radius * Mathd.Sqrt(6)));
        Vertex p3 = new Vertex(center + new Vector3d(-radius * Mathd.Sqrt(2.5), -radius, -radius * Mathd.Sqrt(6)));
        Vertex p4 = new Vertex(center + new Vector3d(radius * Mathd.Sqrt(10), -radius, 0));

        super_tetrahedron = new Tetrahedron(p1, p2, p3, p4);
    }

    public void createSuperTetrahedron()
    {
            Vertex p1 = new Vertex(-2000, -1000, -2000);
            Vertex p2 = new Vertex(2000, -1000, -2000);
            Vertex p3 = new Vertex(0, -1000, 2000);
            Vertex p4 = new Vertex(0, 2000, 0);

            super_tetrahedron = new Tetrahedron(p1, p2, p3, p4);
        //    octree.AddSuperTetraHedron(super_tetrahedron);  
        
    }

    public Tetrahedron LocatePoint(Tetrahedron start, Vertex v) {
        if (start.withinRange(v)) return start;
        Tetrahedron current = start;
        while (!current.withinRange(v))
        { 
            double shortestDist = float.MaxValue;
            Tetrahedron temp = null;
            foreach(Face f in current.faces)
            {
                if (f.left == null || f.right == null)
                {
                    f.DebugFace(0);
                    continue;
                }
                Vector3d n = Vector3d.Cross(f.p3 - f.p1, f.p2 - f.p1).normalized;
                double dist = Vector3d.Dot(current.centerOfMass- f.p1.loc, n);
                if (dist > 0) n *= -1;

                dist = Vector3d.Dot(v - f.p1.loc, n);
                if(dist < shortestDist && dist > 0)
                {
                    shortestDist = dist;
                    temp = (f.left == current ? f.right : f.left);
                } 
            }
            current = temp;
        }
        return current;
    }

    public bool DoStep()
    {
        if (i < points.Count)
        {
       //     GameManager.AddVertex(points[i]);
            badTerahedrons.Clear();
            queue.Clear();
            checke.Clear();

            if (i == 0) {
                badTerahedrons.Add(super_tetrahedron);
                triangulation.RemoveAt(0);  
            }
            else
            {

                //Not working, need to test random walk...
                /*    List<Vertex> nearPoints = octree.GetNeighboringVertices(points[i]);
                    for (int j = 0; j < nearPoints.Count; j++)
                    {
                        foreach (Tetrahedron t in nearPoints[j].neighbors)
                        {
                            checke.Add(t);
                            if (t.withinRange(points[i]))
                            {
                                t.bad = true;
                                badTerahedrons.Add(t);
                                List<Tetrahedron> neigh = t.getNeighbors();
                                neigh.RemoveAll(x => checke.Contains(x));
                                queue.AddRange(neigh);
                                triangulation.Remove(t);
                                break;
                            }
                        }
                        if (queue.Count != 0) break;
                    }*/


                   for (int j = triangulation.Count - 1; j >= 0; j--)
                   {
                       checke.Add(triangulation[j]);
                       if (triangulation[j].withinRange(points[i]))
                       {
                           triangulation[j].bad = true;
                           badTerahedrons.Add(triangulation[j]);
                           List<Tetrahedron> neigh = triangulation[j].getNeighbors();
                           neigh.RemoveAll(x => checke.Contains(x));
                           queue.AddRange(neigh);
                           triangulation.RemoveAt(j);
                           break;
                       }
                   }
              //  queue.Add(LocatePoint(triangulation[0], points[i]));
                while (queue.Count > 0)
                {
                    Tetrahedron t = queue[0];
                    if (t.bad)
                    {
                        queue.RemoveAt(0);
                        continue;
                    }
                    checke.Add(t);
                    if (t.withinRange(points[i]))
                    {
                        t.bad = true;
                        badTerahedrons.Add(t);
                        List<Tetrahedron> neigh = t.getNeighbors();
                        neigh.RemoveAll(x => checke.Contains(x));
                        queue.AddRange(neigh);
                        triangulation.Remove(t);
                    }
                    queue.RemoveAt(0);
                }
            }

            octree.AddPoint(points[i]);
            vertices.Add(points[i]);
            points[i].bw = this;

            List<Tetrahedron> newTetrahedron = new List<Tetrahedron>();
            for (int j = 0; j < badTerahedrons.Count; j++)
            {
                Tetrahedron temp = badTerahedrons[j];
                temp.removeFromVertices();
                for (int k = 0; k < 4; k++)
                {
                    Face e = temp.faces[k];
                   
                    bool bad = (e.left != null && e.left.bad && e.right != null && e.right.bad);

                    if (!bad)
                    {
                        Tetrahedron t = new Tetrahedron(points[i], e);
                        for (int h = 0; h < newTetrahedron.Count; h++)
                        {
                            Face f = newTetrahedron[h].shareFace(t);
                            if (f != null && f != e)
                            {
                                t.faces.Remove(f);
                                t.faces.Add(f);

                                if (f.left == newTetrahedron[h])
                                    f.right = t;
                                else if (f.right == newTetrahedron[h])
                                    f.left = t;
                            }
                        }
                        triangulation.Add(t);
                        newTetrahedron.Add(t);
                    }
                    
                }
            }
            
            i++;
            
            return true;
        }
        else
        {
            triangulation.RemoveAll(t => t.hasVertex(super_tetrahedron.p1) || t.hasVertex(super_tetrahedron.p2) ||
                    t.hasVertex(super_tetrahedron.p3) || t.hasVertex(super_tetrahedron.p4));
            done = true;
        }
        return false;
    }

    public void DrawTriangulation()
    {
            for (int i = 0; i < triangulation.Count; i++)
            {
          //      if (!triangulation[i].hasVertexFrom(controlPoints))
           //     {
                    triangulation[i].DebugTetra(0);
           //     }
            }
        
    }

    public void DrawVoronoi()
    {
        for(int i = 0; i < vertices.Count; i++)
        {
            if (!controlPoints.Contains(vertices[i]))
            {
                vertices[i].DrawCell();
            }
            
        }
   //     for(int i = 0; i < triangulation.Count; i++)
   //     {
   //         triangulation[i].DebugVoronoi();
   //     }
    }

    public void DrawPoints()
    {
        foreach(Vertex p in points)
        {
            Debug.DrawRay(p.loc.vector3, Vector3.up, Color.cyan, 100f);
        }
    }

    public void DrawOctree()
    {
      //  octree.DrawOctree();
    }

    public void MakeDiagram()
    {
        while (!done)
        {
            DoStep();
        }
    }

}
