﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face: System.IEquatable<Face>{

    public Vertex p1,p2,p3;

    public List<Edge<Tetrahedron>> edges;
    public Tetrahedron left;
    public Tetrahedron right;

    public Face(Vertex p1, Vertex p2, Vertex p3, Tetrahedron parent)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        left = parent;

        edges = new List<Edge<Tetrahedron>>();
        edges.Add(new Edge<Tetrahedron>(p1, p2, parent));
        edges.Add(new Edge<Tetrahedron>(p2, p3, parent));
        edges.Add(new Edge<Tetrahedron>(p3, p1, parent));

    }

    public void DebugFace(int xoffset)
    {
        for(int i = 0; i < edges.Count; i++)
        {
            edges[i].DebugLine();
        }
    }

    public bool Equals(Face other)
    {
        if (other == null) return false;
        return ((p1 == other.p1 || p1 == other.p2 || p1 == other.p3) &&
            (p2 == other.p1 || p2 == other.p2 || p2 == other.p3) &&
            (p3 == other.p1 || p3 == other.p2 || p3 == other.p3));
    }

    public bool hasEdge(Edge<Tetrahedron> e)
    {
        return edges.IndexOf(e) != -1;
    }

    public Edge<Tetrahedron> shareEdges(Face other)
    {
        for(int i = 0; i < 3; i++)
        {
            if (other.hasEdge(edges[i]))
            {
                return edges[i];
            }
        }
        return default(Edge<Tetrahedron>);
    }
}
