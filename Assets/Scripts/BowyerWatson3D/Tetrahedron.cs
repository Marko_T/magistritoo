﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetrahedron: System.IEquatable<Tetrahedron>
{

    public Vertex p1, p2, p3, p4;
    public List<Face> faces;
    public List<Edge<Tetrahedron>> edges;

    public Vector3d circumceter;
    public double radius;
    public Vector3d centerOfMass;

    public bool bad = false;

    public void PrintValues()
    {

        Debug.DrawRay(circumceter.vector3, Vector3.up * 100, Color.cyan, 100f);
        Edge<Tetrahedron> a = edges[0];
        Edge<Tetrahedron> b = edges[1];
        a.DebugLine(0);
        b.DebugLine(0);

    //    Debug.DrawLine(a.mid, new Vector3(a.mid.x + 200, a.f * (a.mid.x + 200) + a.g), Color.cyan, 100f);
    //    Debug.DrawLine(b.mid, new Vector3(b.mid.x + 200, b.f * (b.mid.x + 200) + b.g), Color.green, 100f);
   //     Debug.Log("Af: " + a.f + " Bf: " + b.f);
   //     Debug.Log("Ag: " + a.g + " Bg: " + b.g);
    }

    public Tetrahedron(Vertex p1, Vertex p2, Vertex p3, Vertex p4)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;

        p1.AddIncidentTetrahedron(this);
        p2.AddIncidentTetrahedron(this);
        p3.AddIncidentTetrahedron(this);
        p4.AddIncidentTetrahedron(this);

        faces = new List<Face>();
        edges = new List<Edge<Tetrahedron>>();

        faces.Add(new Face(p1, p2, p3, this));
        faces.Add(new Face(p2, p3, p4, this));
        faces.Add(new Face(p3, p4, p1, this));
        faces.Add(new Face(p4, p1, p2, this));

        edges.AddRange(faces[0].edges);
        edges.AddRange(faces[1].edges);
        edges.AddRange(faces[2].edges);
        edges.AddRange(faces[3].edges);

        calculateCircumsphere();

    }

    public Tetrahedron(Vertex p1, Face f)
    {

        this.p1 = p1;
        this.p2 = f.p1;
        this.p3 = f.p2;
        this.p4 = f.p3;

        p1.AddIncidentTetrahedron(this);
        p2.AddIncidentTetrahedron(this);
        p3.AddIncidentTetrahedron(this);
        p4.AddIncidentTetrahedron(this);

        faces = new List<Face>();
        edges = new List<Edge<Tetrahedron>>();

        faces.Add(new Face(p1, p2, p3, this));
        faces.Add(new Face(p3, p1, p4, this));
        faces.Add(new Face(p1, p2, p4, this));

        if (f.right != null && f.right.bad)
            f.right = this;
        else
            f.left = this;

        faces.Add(f);

        edges.AddRange(faces[0].edges);
        edges.AddRange(faces[1].edges);
        edges.AddRange(faces[2].edges);
        edges.AddRange(faces[3].edges);
        calculateCircumsphere();
    }

    private void calculateCircumsphere()
    {
     /*   Matrix4x4 matA = new Matrix4x4();
        matA.SetRow(0, new Vector4(p1.x, p1.y, p1.z, 1));
        matA.SetRow(1, new Vector4(p2.x, p2.y, p2.z, 1));
        matA.SetRow(2, new Vector4(p3.x, p3.y, p3.z, 1));
        matA.SetRow(3, new Vector4(p4.x, p4.y, p4.z, 1));

        float a = matA.determinant;

        Matrix4x4 matDx = new Matrix4x4();
        matDx.SetRow(0, new Vector4(p1.x*p1.x + p1.y*p1.y + p1.z*p1.z, p1.y, p1.z, 1));
        matDx.SetRow(1, new Vector4(p2.x*p2.x + p2.y*p2.y + p2.z*p2.z, p2.y, p2.z, 1));
        matDx.SetRow(2, new Vector4(p3.x*p3.x + p3.y*p3.y + p3.z*p3.z, p3.y, p3.z, 1));
        matDx.SetRow(3, new Vector4(p4.x*p4.x + p4.y*p4.y + p4.z*p4.z, p4.y, p4.z, 1));

        float Dx = matDx.determinant;

        Matrix4x4 matDy = new Matrix4x4();
        matDy.SetRow(0, new Vector4(p1.x * p1.x + p1.y * p1.y + p1.z * p1.z, p1.x, p1.z, 1));
        matDy.SetRow(1, new Vector4(p2.x * p2.x + p2.y * p2.y + p2.z * p2.z, p2.x, p2.z, 1));
        matDy.SetRow(2, new Vector4(p3.x * p3.x + p3.y * p3.y + p3.z * p3.z, p3.x, p3.z, 1));
        matDy.SetRow(3, new Vector4(p4.x * p4.x + p4.y * p4.y + p4.z * p4.z, p4.x, p4.z, 1));

        float Dy = matDy.determinant;

        Matrix4x4 matDz = new Matrix4x4();
        matDz.SetRow(0, new Vector4(p1.x * p1.x + p1.y * p1.y + p1.z * p1.z, p1.x, p1.y, 1));
        matDz.SetRow(1, new Vector4(p2.x * p2.x + p2.y * p2.y + p2.z * p2.z, p2.x, p2.y, 1));
        matDz.SetRow(2, new Vector4(p3.x * p3.x + p3.y * p3.y + p3.z * p3.z, p3.x, p3.y, 1));
        matDz.SetRow(3, new Vector4(p4.x * p4.x + p4.y * p4.y + p4.z * p4.z, p4.x, p4.y, 1));

        float Dz = matDx.determinant;

        Matrix4x4 matC = new Matrix4x4();
        matC.SetRow(0, new Vector4(p1.x * p1.x + p1.y * p1.y + p1.z * p1.z, p1.x, p1.y, p1.z));
        matC.SetRow(1, new Vector4(p2.x * p2.x + p2.y * p2.y + p2.z * p2.z, p2.x, p2.y, p2.z));
        matC.SetRow(2, new Vector4(p3.x * p3.x + p3.y * p3.y + p3.z * p3.z, p3.x, p3.y, p3.z));
        matC.SetRow(3, new Vector4(p4.x * p4.x + p4.y * p4.y + p4.z * p4.z, p4.x, p4.y, p4.z));

        float c = matDx.determinant;

        circumceter = new Vector3(Dx / (2 * a), Dy / (2 * a), Dz / (2 * a));
        radius = Mathf.Sqrt(Dx * Dx + Dy * Dy + Dz * Dz - 4 * a * c) / (2 * Math.Abs(a));

        return;*/

        Vector3d v1 = p2 - p1;
        Vector3d v2 = p3 - p1;
        Vector3d v3 = p4 - p1;

        double l1 = v1.sqrMagnitude;
        double l2 = v2.sqrMagnitude;
        double l3 = v3.sqrMagnitude;

        circumceter = p1 + (l1 * Vector3d.Cross(v2, v3) + l2 * Vector3d.Cross(v3, v1) + l3 * Vector3d.Cross(v1, v2)) / (2 * Vector3d.Dot(v1, Vector3d.Cross(v2, v3)));
        radius = (p1.loc - circumceter).sqrMagnitude;
        circumceter = circumceter * 1000;
        circumceter = new Vector3d((int)circumceter.x / 1000.0, (int)circumceter.y / 1000.0, (int)circumceter.z / 1000.0);

        centerOfMass = (p1.loc + p2.loc + p3.loc + p4.loc) / 4;
    }

    public void removeFromVertices()
    {
        p1.RemoveIncidentTetrahedron(this);
        p2.RemoveIncidentTetrahedron(this);
        p3.RemoveIncidentTetrahedron(this);
        if(p4 != null) p4.RemoveIncidentTetrahedron(this);
    }

    public List<Tetrahedron> getNeighbors()
    {
        List<Tetrahedron> neighbors = new List<Tetrahedron>();
        foreach(Face f in faces){
            if (f.left == this && f.right != null) neighbors.Add(f.right);
            else if (f.right == this && f.left != null) neighbors.Add(f.left);
        }
        return neighbors;
    }

    public bool hasFace(Face e)
    {
     /*   for(int i = 0; i < 4; i++)
        {
            Debug.Log("Compare");
            Debug.Log("p1: " + e.p1 + " p2: " + e.p2 + " p3: " + e.p3);
            Debug.Log("p1: " + faces[i].p1 + " p2: " + faces[i].p2 + " p3: " + faces[i].p3);
            Debug.Log(e == faces[i]);
            Debug.Log(faces.IndexOf(e));
            Debug.Log("Real index: " + i);
        }*/
        return faces.IndexOf(e) != -1;
    }

    public bool hasEdge(Edge<Tetrahedron> e)
    {
        return edges.IndexOf(e) != -1;
    }

    public bool hasVertex(Vertex v)
    {
        return p1 == v || p2 == v || p3 == v || p4 == v;
    }

    public bool hasVertexFrom(List<Vertex> points)
    {
        return points.Contains(p1) || points.Contains(p2) || points.Contains(p3) || points.Contains(p4);
    }

    public bool withinRange(Vertex p)
    {
        return radius > (p.loc - circumceter).sqrMagnitude;
    }


    public bool withinRange(Vector3d p)
    {
        return radius > (p - circumceter).sqrMagnitude;
    }

    public Face shareFace(Tetrahedron t)
    {
        for(int i = 0; i < 4; i++)
        {
            if (t.hasFace(faces[i]))
            {
                return faces[i];
            }
        }
        return null;
    }

    public Edge<Tetrahedron> shareEdge(Tetrahedron t)
    {
        for (int i = 0; i < 3; i++)
        {
            if (t.hasEdge(edges[i]))
            {
                return edges[i];
            }
        }
        return default(Edge<Tetrahedron>);
    }

    public void DrawTriangle()
    {
        for(int i = 0; i < 3; i++)
        {
            edges[i].DebugLine(0);
        }  
    }

    public void DebugTetra(int xoffset)
    {
        for(int i = 0; i < faces.Count; i++)
        {
            faces[i].DebugFace(xoffset);
        }
    }

    public void DebugVoronoi()
    {
        for(int i = 0; i < 4; i++)
        {
            Vector3d point = new Vector3d();
            bool found = false;
            if(faces[i].left == this && faces[i].right != null)
            {
                point = faces[i].right.circumceter;
                found = true;
            }else if(faces[i].left != null && faces[i].right == this)
            {
                point = faces[i].left.circumceter;
                found = true;
            }
            Debug.DrawLine(circumceter.vector3, point.vector3, Color.green);
            /*   if (circumceter.z > 100 || point.z > 100) found = false;
               if (circumceter.z < -200 && point.z < -200) found = false;

               if (circumceter.y > 200 || point.y > 200) found = false;
               if (circumceter.y < -100 || point.y < -100) found = false;


               if (found)
               {
                   Debug.DrawLine(circumceter, point, Color.green);
               }
               */
        }
    }

    public bool Equals(Tetrahedron other)
    {
        if (other == null) return false;
        return ((p1 == other.p1 || p1 == other.p2 || p1 == other.p3 || p1 == other.p4) &&
            (p2 == other.p1 || p2 == other.p2 || p2 == other.p3 || p2 == other.p4) &&
            (p3 == other.p1 || p3 == other.p2 || p3 == other.p3 || p3 == other.p4)&&
            (p4 == other.p1 || p4 == other.p2 || p4 == other.p3 || p4 == other.p4));
    }
}
