﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaussianRandom {

    public static Vector3d NextGaussianPoint(Vector3d center, float radius)
    {
        Vector3d point = new Vector3d(Random.insideUnitSphere);

        double s = point.magnitude;
  //      point *= -2* Mathd.Log(s);

        return point = point * radius + center;
    }
}
