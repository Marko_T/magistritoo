﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowyerWatson2D{

    public List<Vertex> points;
    public Triangle SuperTriangle;

    public List<Triangle> triangulation;

    private int i = 0;

    private List<Triangle> badTriangles = new List<Triangle>();
    private List<Triangle> queue = new List<Triangle>();
    private List<Triangle> checke = new List<Triangle>();

    private bool done = false;
    private Planed plane;

    private Quaternion rotQuat;
    private Quaternion inverseQuat;
    private Vector3d translation;

    public static bool drawn = false;

    private double zOffset;
    public BowyerWatson2D(List<Vertex> points, Planed plane, Vector3d center)
    {
        this.points = points;
        this.plane = plane;
        CreateRotationMatrix(plane);
        generateSuperTriangle(center);
        triangulation = new List<Triangle>();
        triangulation.Add(SuperTriangle);
    }

    public BowyerWatson2D(List<Triangle> triangulation)
    {
        this.triangulation = triangulation;
        points = new List<Vertex>();
    }
    
    private void CreateRotationMatrix(Planed p)
    {
        Vector3 n = p.normal.vector3;

        double cosA = Vector3d.Dot(p.normal, Vector3d.forward);

        rotQuat = Quaternion.AngleAxis((float)Mathd.Acos(cosA) * Mathf.Rad2Deg, Vector3.Cross(n, Vector3.forward));
        inverseQuat = Quaternion.Inverse(rotQuat);

        translation = new Vector3d(0, 0, -p.distance / n.z);
    }
    private void generateSuperTriangle(Vector3d center)
    {
        Vector3d moved = new Vector3d(rotQuat * center.vector3);
        Vertex p1 = new Vertex(Vector3d.up * 3000 + moved);
        Vertex p2 = new Vertex(new Vector3d(Quaternion.AngleAxis(120, Vector3.forward) * Vector3.up * 3000f) + moved);
        Vertex p3 = new Vertex(new Vector3d(Quaternion.AngleAxis(240, Vector3.forward) * Vector3.up * 3000f) + moved);

        SuperTriangle = new Triangle(p1, p2, p3);
    }

    public void MakeDiagram()
    {
        while (!done)
        {
            doStep();
        }
    }

    public bool AddPoint(Vector3d point)
    {
        int index = points.FindIndex(x => (x - point).sqrMagnitude < 0.01);
        if (index == -1)
        {
            points.Add(new Vertex(new Vector3d(inverseQuat*  point.vector3)));
            doStep();
            return true;
        }
        return false;
    }

    private bool doStep()
    {
        if (i < points.Count)
        {

            points[i].loc = new Vector3d(rotQuat * points[i].loc.vector3);
        //    Debug.Log(points[i].loc);
            badTriangles.Clear();
            for (int j = triangulation.Count - 1; j >= 0; j--)
            {
                if (triangulation[j].withinRange(points[i]))
                {
                    triangulation[j].bad = true;
                    badTriangles.Add(triangulation[j]);
                    triangulation.RemoveAt(j);
                }
            }

            List<Triangle> newTriangles = new List<Triangle>();
            for (int j = 0; j < badTriangles.Count; j++)
            {
                Triangle temp = badTriangles[j];
                for (int k = 0; k < 3; k++)
                {
                    Edge<Triangle> e = temp.edges[k];
                    bool bad = e.left != null && e.left.bad && e.right != null && e.right.bad;

                    if (!bad)
                    {
                        Triangle t = new Triangle(points[i], e, temp);
                        t.normal = plane.normal;

                        for (int h = 0; h < newTriangles.Count; h++)
                        {
                            Edge<Triangle> f = newTriangles[h].shareEdge(t);
                            if (f != null && f != e)
                            {
                                t.edges.Remove(f);
                                t.edges.Add(f);

                                if (f.left == newTriangles[h])
                                    f.right = t;
                                else if (f.right == newTriangles[h])
                                    f.left = t;
                            }
                        }
                        triangulation.Add(t);
                        newTriangles.Add(t);
                    }
                }
            }
            i++;
            return true;
        }
        else
        {
            drawn = true;
            triangulation.RemoveAll(t => t.hasVertex(SuperTriangle.p1) || t.hasVertex(SuperTriangle.p2) ||
                    t.hasVertex(SuperTriangle.p3));
            foreach(Triangle t in triangulation)
            {
                for(int j = 0; j < 3; j++)
                {
                    Edge<Triangle> e = t.edges[j];
               
                    if(e.left == t && !triangulation.Contains(e.right))
                    {
                        e.right = null;
                    }else if(e.right == t && !triangulation.Contains(e.left))
                    {
                        e.left = null;
                    }
                }
            }
            foreach(Vertex v in points)
            {
                v.loc = new Vector3d(inverseQuat * v.loc.vector3);
            }
            done = true;
        }
        return false;
    }


    public void RotatePoints()
    {
        foreach (Vertex v in points)
        {
            v.loc = new Vector3d(rotQuat * v.loc.vector3);
        }
    }

    public void InverseRot()
    {
        foreach (Vertex v in points)
        {
            v.loc = new Vector3d(inverseQuat * v.loc.vector3);
        }
    }
}
