﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Triangle : System.IEquatable<Triangle>
{

    public Vertex p1, p2, p3;
    public List<Edge<Triangle>> edges;

    public Vector3d circumceter;
    public Vector3d centerOfMass;
    public double radius;

    public Vector3d normal;
    private double areaTimes2;
    private double area;

    public bool bad = false;

    public bool keep = false;
    public bool processed = false;

    public Triangle(Vertex p1, Vertex p2, Vertex p3)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;

        edges = new List<Edge<Triangle>>();

        edges.Add(new Edge<Triangle>(p1, p2, this));
        edges.Add(new Edge<Triangle>(p2, p3, this));
        edges.Add(new Edge<Triangle>(p3, p1, this));

        /*
         * y = f*x +g
         * 
         * f1*x +g1 = f2*x+g2
         * (f1-f2)x = g2-g1
         * x = (g2-g1)/(f1-f2)
         */

     //   Edge<Triangle> a = edges[0];
     //   Edge<Triangle> b = edges[1];

     /*   float x = 0, y = 0;
        if (float.IsInfinity(a.f))
        {
            x = a.mid.x;
            y = b.f * x + b.g;
        }
        else if (float.IsInfinity(b.f))
        {
            x = b.mid.x;
            y = a.g * x + b.g;
        }
        else
        {
            x = (b.g - a.g) / (a.f - b.f);
            y = b.f * x + b.g;
        }
        circumceter = new Vector3(x, y);
        radius = Vector3.Distance(p1.loc, circumceter);*/
        calculateCircumcenter();

    }

    public Triangle(Vertex p1, Edge<Triangle> e, Triangle old)
    {
        this.p1 = p1;
        this.p2 = e.start;
        this.p3 = e.end;

        edges = new List<Edge<Triangle>>();

        edges.Add(e);
        edges.Add(new Edge<Triangle>(p1, p2, this));
        edges.Add(new Edge<Triangle>(p1, p3, this));

        if (e.left == old)
        {
            e.left = this;
        }
        else if (e.right == old)
        {
            e.right = this;
        }


    //    Edge<Triangle> a = edges[0];
    //    Edge<Triangle> b = edges[1];
   /*     float x = 0, y = 0;
        if (float.IsInfinity(a.f))
        {
            x = a.mid.x;
            y = b.f * x + b.g;
        }
        else if (float.IsInfinity(b.f))
        {
            x = b.mid.x;
            y = a.g * x + b.g;
        }
        else
        {
            x = (b.g - a.g) / (a.f - b.f);
            y = b.f * x + b.g;
        }
        circumceter = new Vector3(x, y);

        radius = Vector3.Distance(p1.loc, circumceter);
        */
        calculateCircumcenter();
    }

    public List<Triangle> Neighbours()
    {
        List<Triangle> neigbours = new List<Triangle>();
        foreach(Edge<Triangle> e in edges)
        {
            if (e.left == this && e.right != null) neigbours.Add(e.right);
            if (e.right == this && e.left != null) neigbours.Add(e.left);
        }
        return neigbours;
    }

    private void calculateCircumcenter()
    {

        Edge<Triangle> a = edges[0];
        Edge<Triangle> b = edges[1];
         double x = 0, y = 0;
         if (double.IsInfinity(a.f))
         {
             x = a.mid.x;
             y = b.f * x + b.g;
         }
         else if (double.IsInfinity(b.f))
         {
             x = b.mid.x;
             y = a.g * x + b.g;
         }
         else
         {
             x = (b.g - a.g) / (a.f - b.f);
             y = b.f * x + b.g;
         }
         circumceter = new Vector3d(x, y);
        radius = (new Vector3d(p1.x, p1.y) - circumceter).sqrMagnitude;

        double s = (edges[0].length + edges[1].length + edges[2].length) / 2;
        area = Mathd.Sqrt(s * (s - edges[0].length) * (s - edges[1].length) * (s - edges[2].length));

/*
         Vector3d ac = p3 - p1;
         Vector3d ab = p2 - p1;
         Vector3d abXac = Vector3d.Cross(ab, ac);
         */
   //      circumceter = p1 + (Vector3d.Cross(abXac, ab) * ac.sqrMagnitude + Vector3d.Cross(ac, abXac) * ab.sqrMagnitude) / (2 * abXac.sqrMagnitude);
   //      radius = (p1.loc -circumceter).sqrMagnitude;

   //      areaTimes2 = abXac.magnitude;
         centerOfMass = (p1.loc + p2.loc + p3.loc) / 3;
     }

     public bool withinRange(Vertex p)
     {
        return radius - (new Vector3d(p.x, p.y) - circumceter).sqrMagnitude > 0;
     //    Vector3d vec = circumceter - p.loc;
     //    double dist = Vector3d.Dot(vec, normal);
     //    Vector3d projection = vec - dist * normal;
     //    return radius - projection.sqrMagnitude >= 0;
     }

     public Edge<Triangle> shareEdge(Triangle t)
     {
         for (int i = 0; i < 3; i++)
         {
             if (t.hasEdge(edges[i]))
             {
                 return edges[i];
             }
         }
         return default(Edge<Triangle>);
     }

     public bool hasVertex(Vertex v)
     {
         return p1 == v || p2 == v || p3 == v;
     }

     public bool hasEdge(Edge<Triangle> e)
     {
         return edges.IndexOf(e) != -1;
     }

     public bool pointInTriangle(Vector3d point)
     {
         double a = Vector3d.Cross(p2 - point, p3 - point).magnitude / areaTimes2;
         double b = Vector3d.Cross(p3 - point, p1 - point).magnitude / areaTimes2;
         double c = 1 - a - b;

         return 0 <= a && a <= 1 && 0 <= b && b <= 1 && 0 <= c && c <= 1;
     }

     public bool Equals(Triangle other)
     {
         if (other == null) return false;
         return ((p1 == other.p1 || p1 == other.p2 || p1 == other.p3) &&
             (p2 == other.p1 || p2 == other.p2 || p2 == other.p3) &&
             (p3 == other.p1 || p3 == other.p2 || p3 == other.p3));
     }

     public void DebugTriangle(Vector3d loc)
     {
         Debug.DrawLine((p1 + loc).vector3, (p2 + loc).vector3, Color.blue, 100f);
         Debug.DrawLine((p2 + loc).vector3, (p3 + loc).vector3, Color.blue, 100f);
         Debug.DrawLine((p3 + loc).vector3, (p1 + loc).vector3, Color.blue, 100f);
     }
 }
