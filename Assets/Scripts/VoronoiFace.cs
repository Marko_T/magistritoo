﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VoronoiFace : AVoronoiFace
{

    public VoronoiFace(List<Vertex> points, Planed p, Vector3d centerOfMass, Vector3d loc)
    {
        normal = p.normal;
        this.points = points;
        this.centerOfMass = centerOfMass;
        this.loc = loc;
        this.p = p;

        this.cutLinePoints = new List<List<Vertex>>();
        this.cutLines = new List<List<Line>>();

        FindEdges();
        CalculateArea();
    }

    public override void CreateObject(Transform parent, int j, Vertex Vparent)
    {
        this.index = j;
        if (triangulation == null)
        {
            BowyerWatson2D bw = new BowyerWatson2D(points, p, centerOfMass);
            bw.MakeDiagram();
            triangulation = bw.triangulation;
        }
        this.parent = parent;
        Mesh mesh = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();

        for (int i = 0; i < triangulation.Count; i++)
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(triangulation[i].p1.loc.vector3);
            points.Add(triangulation[i].p2.loc.vector3);
            points.Add(triangulation[i].p3.loc.vector3);
            if ((Vector3.Cross(points[1] - points[0], points[2] - points[0]).normalized + p.normal.vector3).sqrMagnitude > 2)
            {
                points.Reverse();
            }
            vertices.AddRange(points);

            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 2);
        }

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        go = new GameObject();
        go.transform.parent = parent;
        go.AddComponent<MeshFilter>().mesh = mesh;
        go.AddComponent<CellDelegate>().vertex = Vparent;
        go.GetComponent<CellDelegate>().face = this;
        go.GetComponent<CellDelegate>().index = this.index;
        go.AddComponent<MeshCollider>().sharedMesh = mesh;
        //     go.GetComponent<MeshCollider>().inflateMesh = true;
        //     go.GetComponent<MeshCollider>().skinWidth = 0.01f;
        //     go.GetComponent<MeshCollider>().convex = true;
        go.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
    }

    public override void UpdateObject()
    {
        if (go == null) return;
        Mesh mesh = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();
        _edges = new List<Edge<Triangle>>();
        for (int i = 0; i < triangulation.Count; i++)
        {
            foreach (Edge<Triangle> e in triangulation[i].edges)
            {
                if (e.left == null || e.right == null)
                {
                    _edges.Add(e);
                }
            }
            List<Vector3> points = new List<Vector3>();
            points.Add(triangulation[i].p1.loc.vector3);
            points.Add(triangulation[i].p2.loc.vector3);
            points.Add(triangulation[i].p3.loc.vector3);
            if ((Vector3.Cross(points[1] - points[0], points[2] - points[0]).normalized + p.normal.vector3).sqrMagnitude > 2)
            {
                points.Reverse();
            }
            vertices.AddRange(points);

            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 2);
        }

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        go.GetComponent<MeshFilter>().mesh = mesh;
        go.GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    public override void RemoveObject()
    {
        GameObject.DestroyObject(go);
    }
}

