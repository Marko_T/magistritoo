﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleVoronoiFace : AVoronoiFace
{

    public InvisibleVoronoiFace(List<Vertex> points, Planed p, Vector3d centerOfMass, Vector3d loc, VoronoiFace child)
    {
        normal = p.normal;
        this.points = points;
        this.centerOfMass = centerOfMass;
        this.loc = loc;
        this.p = p;
        this.child = child;

        this.cutLinePoints = new List<List<Vertex>>();
        this.cutLines = new List<List<Line>>();

        FindEdges();
        CalculateArea();
    }


    public override void CreateObject(Transform parent, int j, Vertex Vparent)
    {
        if (child != null)
        {
            child.CreateObject(parent,j, Vparent);
        }
    }

    public override void RemoveObject()
    {
        if (child != null)
        {
            child.RemoveObject();
            child = null;
        }
    }

    public override void UpdateObject()
    {
        if (child != null)
        {
            child.UpdateObject();
        }
    }

    public override bool HasObject()
    {
        if(child != null)
        {
            return child.HasObject();
        }
        return go != null;
    }
}
