﻿using UnityEngine;

public class Matrix3X3
{
    public float a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, i = 0;

    public Matrix3X3(float a, float b, float c, float d, float e, float f, float g, float h, float i)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
    }

    public static Vector3 operator *(Matrix3X3 m, Vector3 v)
    {
        return new Vector3(m.a * v.x + m.d * v.y + m.g * v.z, m.b * v.x + m.e * v.y + m.h * v.z, m.c * v.x + m.f * v.y + m.i * v.z);
    }

    public static Vector3 operator *(Vector3 v, Matrix3X3 m)
    {
        return new Vector3(v.x * m.a + v.y * m.b + v.z * m.c, v.x * m.d + v.y * m.e + v.z * m.f, v.x * m.g + v.y * m.h + v.z * m.i);
    }

    /*
     * a d g
     * b e h 
     * c f i
     */

    public Matrix3X3 Inverse()
    {
        float aMin = e * i - h * f;
        float dMin = -(b * i - h * c);
        float gMin = b * f - e * c;

        float bMin = -(d * i - g * f);
        float eMin = a * i - g * c;
        float hMin = -(a * f - d * c);

        float cMin = d * h - g * e;
        float fMin = -(a * h - g * b);
        float iMin = a * e - d * b;

        float inverseDetOrigin = 1 / (a * aMin + d * dMin + g * gMin);

        return new Matrix3X3(inverseDetOrigin * aMin, inverseDetOrigin * bMin, inverseDetOrigin * cMin,
            inverseDetOrigin * dMin, inverseDetOrigin * eMin, inverseDetOrigin * fMin,
            inverseDetOrigin * gMin, inverseDetOrigin * hMin, inverseDetOrigin * iMin);
    }
}