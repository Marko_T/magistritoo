﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DebugVertices : MonoBehaviour {

    public Mesh mesh;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        if(mesh == null)
        {
            mesh = GetComponent<MeshFilter>().mesh;
        }

        foreach(Vector3 v in mesh.vertices)
        {
            Handles.Label(v + transform.parent.position, "(" + (v.x + transform.parent.position.x) + ", " + (v.y + transform.parent.position.y) + ", " + (v.z + +transform.parent.position.z) + ")");
        }
    }
}
