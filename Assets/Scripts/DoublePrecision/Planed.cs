﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
    public struct Planed
    {

        private Vector3d _Normal;
        private double _Distance;

        public Vertex left;
        public Vertex right;
        public Vector3d normal
        {
            get
            {
                return this._Normal;
            }set
            {
                this._Normal = value;
            }
        }

        public double distance
        {
            get
            {
                return this._Distance;
            }set
            {
                this._Distance = value;
            }
        }

        public Planed(Vector3d inNormal, Vector3d inPoint, Vertex left, Vertex right)
        {
            this._Normal = Vector3d.Normalize(inNormal);
            this._Distance = -Vector3d.Dot(inNormal, inPoint);
            this.left = left;
            this.right = right;
        }

        public bool GetSide(Vector3d inPt)
        {
            return Vector3d.Dot(this.normal, inPt) + this.distance > 0.0;
        }

        public double GetDistanceToPoint(Vector3d inPt)
        {
            return Vector3d.Dot(this.normal, inPt) + this.distance;
        }
    }
}
