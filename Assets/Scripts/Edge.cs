﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge<T>: System.IEquatable<Edge<T>>
{

    public Vertex start;
    public Vertex end;

    public Vector3d mid;

    public T left;
    public T right;

    public double f;
    public double g;

    public double length;


    public Edge(Vertex left, Vertex right, T parent)
    {
        this.left = parent;
        this.right = default(T);
        start = left;
        end = right;
        length = (end - start).magnitude;
    //    double m = (right.x - left.x) / (left.y - right.y);

        mid = (start + end) / 2;
        /*
         * y - y1 = m * (x - x1) 
         * y = m*x - m*x1 +y1
         * f = m
         * g = m*x1 +y1
         */

        double m = (right.y - left.y) / (right.x - left.x);
        m = -1 / m;

        f = m;
        g = mid.y - f * mid.x;
    }

    public bool hasVertex(Vertex p)
    {
        return start.Equals(p) || end.Equals(p);
    }

    public void DebugLine(int xoffset)
    {
        Debug.DrawLine((start.loc.vector3 + new Vector3(xoffset,0,0)), (end.loc.vector3+new Vector3(xoffset,0,0)), Color.red);
    }

    public void DebugLine()
    {
        Debug.DrawLine((start.loc.vector3), (end.loc.vector3), Color.green, 100f);
    }

    public bool Equals(Edge<T> other)
    {
        return (start-other.start).sqrMagnitude < 0.001 && (end-other.end).sqrMagnitude < 0.001 || (start - other.end).sqrMagnitude < 0.001 && (end - other.start).sqrMagnitude < 0.001;
    }

    public override bool Equals(object obj)
    {
        if (obj == null) return false;
        return this.Equals((Edge<T>)obj);
    }
}