﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static List<Vertex> materialized = new List<Vertex>();
    public static List<Vertex> allVertices = new List<Vertex>();
    public static List<Vertex> voronoiVertices = new List<Vertex>();

    public static List<Vector3d> cutLinePoints = new List<Vector3d>();

    public static int AddVertex(Vertex v)
    {
        int index = allVertices.FindIndex(x => (x - v).sqrMagnitude < 0.01);
        if (index == -1)
        {
            allVertices.Add(v);
            return allVertices.Count - 1;
        }
        return index;
    }
}
