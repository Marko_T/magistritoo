﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class AVoronoiFace : System.IEquatable<AVoronoiFace>
{
    public List<Vertex> points;
    protected List<Edge<Triangle>> _edges;
    private List<Edge<Triangle>> _cutlineEdges;
    public Vector3d normal;
    public List<List<Vertex>> cutLinePoints;
    public List<List<Line>> cutLines;
    public int index = 0;
    protected double area;

    public List<Edge<Triangle>> Edges
    {
        get
        {
            return _edges.Concat(_cutlineEdges).ToList();
        }
    }
 
    public VoronoiFace child;
    public double Area
    {
        get
        {
            return area;
        }
    }

    public Quaternion rotationToXYPlane
    {
        get
        {
            return Quaternion.AngleAxis((float)Mathd.Acos(Vector3d.Dot(p.normal, Vector3d.forward)) * Mathf.Rad2Deg, Vector3.Cross(p.normal.vector3, Vector3.forward));
        }
    }

    public Quaternion inverseRotationToXYPLane
    {
        get
        {
            return Quaternion.Inverse(rotationToXYPlane);
        }
    }
    protected List<Triangle> triangulation;
    public Vector3d centerOfMass;

    public Planed p;

    protected Transform parent;
    public Vector3d loc;

    protected GameObject go;

    public abstract void CreateObject(Transform parent, int j, Vertex Vparent);
    public abstract void UpdateObject();
    public abstract void RemoveObject();


    protected void FindEdges()
    {
        SortPoints();
        _edges = new List<Edge<Triangle>>();
        _cutlineEdges = new List<Edge<Triangle>>();

        for (int i = 0; i < points.Count; i++)
        {
            _edges.Add(new Edge<Triangle>(points[i], points[(i + 1) % points.Count], null));
        }
    }

    public void FindEdgesFromTriagulation()
    {
        _edges = new List<Edge<Triangle>>();
        foreach(Triangle t in triangulation)
        {
            foreach(Edge<Triangle> e in t.edges)
            {
                if(e.left == null || e.right == null)
                {
                    _edges.Add(e);
                }
            }
        }
    }

    private void SortPoints()
    {
        Vector3d refrence = points[0] - centerOfMass;
        points = points.OrderBy(x => Vector3.SignedAngle(x.loc.vector3 - centerOfMass.vector3, refrence.vector3, normal.vector3)).ToList();
    }

    protected void CalculateArea()
    {
        area = 0;
        for (int i = 0; i < points.Count - 1; i++)
        {
            Vector3d a = points[i] - centerOfMass;
            Vector3d b = points[i + 1] - centerOfMass;
            double dot = Vector3d.Dot(a.normalized, b.normalized);
            area += a.magnitude * b.magnitude * Math.Sqrt(1 - dot * dot);
        }
        /*      foreach(Triangle t in triangulation)
              {
                  Vector3 a = t.p2 - t.p1;
                  Vector3 b = t.p3 - t.p1;
                  float dot = Vector3.Dot(a.normalized, b.normalized);
                  area += a.magnitude * b.magnitude * Mathf.Sqrt(1 - dot * dot);
              }*/
        area = area / 2;
    }

    public double getVolumeValue()
    {
        return Vector3d.Dot(centerOfMass, normal) * area;
    }

    public bool pointOnPlane(Vector3d point)
    {
        return Math.Abs(p.GetDistanceToPoint(point)) < 0.0005;
    }

    public List<Triangle> getTriangulation()
    {
        return triangulation;
    }

    public void setTriangulation(List<Triangle> triangulation)
    {
        this.triangulation = triangulation;
    }

    public void AddCutlines(List<Line> cutLines, List<Line> cutLines2)
    {
        List<Line> processed = new List<Line>();
        processed.AddRange(cutLines);

        if (this.cutLines.Count > 0)
        {
            List<Vertex> cutlinePoints = this.cutLinePoints[this.cutLinePoints.Count - 1];
            for (int j = 0; j < this.cutLines.Count; j++)
            {
                for (int i = this.cutLines[j].Count - 1; i >= 0; i--)
                {
                    Line l = this.cutLines[j][i];
                    bool startInside = PointInConvex(cutLines2, l.start);
                    bool endInside = PointInConvex(cutLines2, l.end);
                    if (startInside && endInside)
                    {
                        this.cutLines[j].RemoveAt(i);
                        this.cutLinePoints[j].Remove(new Vertex(l.start - loc));
                        this.cutLinePoints[j].Remove(new Vertex(l.end - loc));

                        continue;
                    }
                    else if (startInside)
                    {
                        foreach (Line g in cutLines)
                        {
                            Vector3d intersection = CellMerger.LineLineIntersection(g, l);
                            if (!double.IsInfinity(intersection.x))
                            {
                                this.cutLinePoints[j].Remove(new Vertex(l.start - loc));
                                this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                                l.start = intersection;

                                if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                                if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                                break;
                            }
                        }
                        this.cutLines[j][i] = l;
                        l.AddOverlap(processed);
                    }
                    else if (endInside)
                    {
                        foreach (Line g in cutLines)
                        {
                            Vector3d intersection = CellMerger.LineLineIntersection(l, g);
                            if (!double.IsInfinity(intersection.x))
                            {
                                this.cutLinePoints[j].Remove(new Vertex(l.end - loc));
                                this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                                l.end = intersection;

                                if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                                if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                                break;
                            }
                        }
                        this.cutLines[j][i] = l;
                        l.AddOverlap(processed);
                    }
                    else
                    {
                        List<Vector3d> intersections = new List<Vector3d>();
                        foreach (Line g in cutLines)
                        {
                            Vector3d intersection = CellMerger.LineLineIntersection(g, l);
                            if (!double.IsInfinity(intersection.x))
                            {
                                intersections.Add(intersection);
                                this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                                if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                                if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                            }
                        }
                        if (intersections.Count > 0)
                        {

                            intersections.Add(l.start);
                            intersections.Add(l.end);
                            intersections = intersections.OrderBy(x => x.x).ThenBy(x => x.y).ThenBy(x => x.z).ToList();
                            this.cutLines[j].RemoveAt(i);
                            for (int k = 0; k < intersections.Count - 1; k += 2)
                            {
                                this.cutLines[j].Add(new Line(intersections[k], intersections[k + 1], intersections[k + 1] - intersections[k]));
                                new Line(intersections[k], intersections[k + 1], intersections[k + 1] - intersections[k]).AddOverlap(processed);
                            }
                        }
                        else
                        {
                            l.AddOverlap(processed);
                        }
                    }
                }
            }
        }    
       
        this.cutLines.Add(cutLines);

        processed.RemoveAll(x => x.overlaps.Count > 1);
        _cutlineEdges.Clear();
        foreach (Line l in processed)
        {
            _cutlineEdges.Add(new Edge<Triangle>(new Vertex(l.start - loc), new Vertex(l.end - loc), null));
        }
    }

    public void JoinCutlines(List<Line> cutlines2)
    {
        List<Line> processed = new List<Line>();
       
        List<Line> lastAdded = cutLines[cutLines.Count - 1];
        processed.AddRange(lastAdded);
        if (cutLines.Count > 1)
        {
            //TODO: muuta ainult cutline edge
            List<Vertex> cutlinePoints = this.cutLinePoints[this.cutLinePoints.Count - 1];
            for (int j = 0; j < this.cutLines.Count-1; j++)
            {
                for (int i = this.cutLines[j].Count - 1; i >= 0; i--)
                {
                    Line l = this.cutLines[j][i];
                        bool startInside = PointInConvex(cutlines2, l.start);
                        bool endInside = PointInConvex(cutlines2, l.end);
                        if (startInside && endInside)
                        {
                        //    this.cutLines[j].RemoveAt(i);
                            this.cutLinePoints[j].Remove(new Vertex(l.start - loc));
                            this.cutLinePoints[j].Remove(new Vertex(l.end - loc));

                            continue;
                        }
                        else if (startInside)
                        {

                            foreach (Line g in lastAdded)
                            {
                                Vector3d intersection = CellMerger.LineLineIntersection(g, l);
                                if (!double.IsInfinity(intersection.x))
                                {
                                    this.cutLinePoints[j].Remove(new Vertex(l.start - loc));
                                    this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                                    //l.start = intersection;

                                    //if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                                    //if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                                    break;
                                }
                            }
                        //    this.cutLines[j][i] = l;
                            l.AddOverlap(processed);
                        }
                        else if (endInside)
                        {
                            foreach (Line g in lastAdded)
                            {
                                Vector3d intersection = CellMerger.LineLineIntersection(l, g);
                                if (!double.IsInfinity(intersection.x))
                                {
                                    this.cutLinePoints[j].Remove(new Vertex(l.end - loc));
                                    this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                              //      l.end = intersection;

                              //      if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                              //      if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                                    break;
                                }
                            }
                       //     this.cutLines[j][i] = l;
                            l.AddOverlap(processed);
                        }
                        else
                        {
                            List<Vector3d> intersections = new List<Vector3d>();
                            foreach (Line g in lastAdded)
                            {
                                Vector3d intersection = CellMerger.LineLineIntersection(g, l);
                                if (!double.IsInfinity(intersection.x))
                                {
                                    intersections.Add(intersection);
                                    this.cutLinePoints[j].Add(new Vertex(intersection - loc));
                                    if ((g.start - intersection).sqrMagnitude < 0.001) g.start = intersection;
                                    if ((g.end - intersection).sqrMagnitude < 0.001) g.end = intersection;
                                }
                            }
                        /*    if (intersections.Count > 0)
                            {

                                intersections.Add(l.start);
                                intersections.Add(l.end);
                                intersections = intersections.OrderBy(x => x.x).ThenBy(x => x.y).ThenBy(x => x.z).ToList();
                                this.cutLines[j].RemoveAt(i);
                                for (int k = 0; k < intersections.Count - 1; k += 2)
                                {
                                 //   this.cutLines[j].Add(new Line(intersections[k], intersections[k + 1], intersections[k + 1] - intersections[k]));
                                    new Line(intersections[k], intersections[k + 1], intersections[k + 1] - intersections[k]).AddOverlap(processed);
                                }
                            }
                            else
                            {
                         //       l.AddOverlap(processed);
                            }*/

                        }
                 //   l.AddOverlap(processed);
                }
            }
        }

   /*     processed.RemoveAll(x => x.overlaps.Count > 1);
        _cutlineEdges.Clear();
        foreach (Line l in processed)
        {
            _cutlineEdges.Add(new Edge<Triangle>(new Vertex(l.start - loc), new Vertex(l.end - loc), null));
        }
    */
    }

    public void CleanEdges()
    {
        _edges.RemoveAll(x => !points.Contains(x.start) && !points.Contains(x.end));
        foreach (Edge<Triangle> e in _edges)
        {
            if (!points.Contains(e.start))
            {
                foreach (Edge<Triangle> l in _cutlineEdges)
                {
                    Vector3d intersection = CellMerger.LineLineIntersection(l, e);
                    if (CellMerger.PointOnSegment(e.start.loc, e.end.loc, intersection) )
                    {
                        e.start = new Vertex(intersection);
                    }
                }
            }

            else if (!points.Contains(e.end))
            {
                foreach (Edge<Triangle> l in _cutlineEdges)
                {
                    Vector3d intersection = CellMerger.LineLineIntersection(l, e);
                    if (CellMerger.PointOnSegment(e.start.loc, e.end.loc, intersection))
                    {
                        e.end = new Vertex(intersection);
                    }
                }
            }
        }
    }

    public bool PointInConvex(List<Line> lines, Vector3d point)
    {
        Vector3d centerOfMass = Vector3d.zero;
        List<Vector3d> points = new List<Vector3d>();
        
        foreach(Line l in lines)
        {
       //     Debug.DrawLine((l.start).vector3, (l.end).vector3, Color.blue, 10f);    
            centerOfMass = l.start + centerOfMass;
            centerOfMass = l.end + centerOfMass;
            points.Add(l.start);
            points.Add(l.end);
        }

     //   points = points.Distinct(new VectorComparer()).ToList();
        centerOfMass = centerOfMass / points.Count;

      //  Vector3d projection = point - Vector3d.Dot(point - centerOfMass, normal) * normal;
        Vector3d refrence = points[0] - centerOfMass;

        List<Vector3d> sorted = points.OrderBy(x => Vector3.SignedAngle(refrence.vector3, x.vector3 - centerOfMass.vector3, normal.vector3)).ToList();

        double sum = 0;

        for(int i = 0; i < sorted.Count; i++)
        {
            sum += Vector3d.Angle(sorted[i] - point, sorted[(i+1)%sorted.Count]- point);
        }

    //    Debug.Log(Math.Abs(sum - 360));
        return Math.Abs(sum - 360) < 1;
    }

    public virtual bool HasObject()
    {
        return go != null;
    }

    public void DrawFace(Vector3d loc)
    {
        foreach (Edge<Triangle> e in _edges)
        {
            Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.cyan, 10f);
        }

        foreach (Edge<Triangle> e in _cutlineEdges)
        {
            Debug.DrawLine(e.start.loc.vector3 + loc.vector3, e.end.loc.vector3 + loc.vector3, Color.cyan, 10f);
        }
    }

    public void MarkFace(Vector3d loc)
    {
        Debug.DrawLine(_edges[0].start.loc.vector3 + loc.vector3, centerOfMass.vector3 + loc.vector3, Color.cyan, 100f);
    }


    public bool Equals(AVoronoiFace other)
    {
        if (other == null) return false;
        List<Vector3d> a = points.ConvertAll(x => x.loc + loc).Distinct(new VectorComparer()).ToList();
        List<Vector3d> b = other.points.ConvertAll(x => x.loc + other.loc).Distinct(new VectorComparer()).ToList();

        return a.All(x => b.FindIndex(y => (x-y).sqrMagnitude < 0.02) != -1) && a.Count == b.Count;
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as AVoronoiFace);
    }
}
