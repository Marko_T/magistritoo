﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DrawTriangulation))]
public class DrawTriangulationInspector : Editor {
    DrawTriangulation script;

    private void Awake()
    {
        script = (DrawTriangulation)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if(GUILayout.Button("Draw triangulation"))
        {
            script.drawTriangulation();
        }

        if(GUILayout.Button("Draw voronoi"))
        {
            script.drawVoronoi();
        }

        if (GUILayout.Button("Draw vertices"))
        {
            script.drawPoints();
        }

        if (GUILayout.Button("Create objects"))
        {
            script.createObjects();
        }

        if(GUILayout.Button("Draw octree"))
        {
            script.drawOctree();
        }
    }
}
