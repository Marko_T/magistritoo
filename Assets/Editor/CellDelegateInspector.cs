﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CellDelegate))]
public class CellDelegateInspector : Editor
{
    CellDelegate script;

    private void Awake()
    {
        script = (CellDelegate)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Draw triangulation"))
        {
            script.DrawTriangulation();
        }
    }
}
