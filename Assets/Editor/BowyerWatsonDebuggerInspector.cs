﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BowyerWatsonDebugger))]
public class BowyerWatsonDebuggerInspector : Editor {

    BowyerWatsonDebugger script;

    private void Awake()
    {
        script = (BowyerWatsonDebugger)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Do step"))
        {
            script.DoStep();
        }

        if (GUILayout.Button("Draw triangulation"))
        {
            script.DrawTriangulation();
        }
    }
}
