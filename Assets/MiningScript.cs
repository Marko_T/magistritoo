﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiningScript : MonoBehaviour {

    int count = 0;
    Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            CastRay();
        }
    }

    private void CastRay()
    {
        RaycastHit hit;

        Ray r = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        
        if (Physics.Raycast(r, out hit))
        {
            Vector3d point = new Vector3d(-80.9f, 222.0f, -8.4f);
            Vector3d point2 = new Vector3d(-79.4f, 222.6f, -7.8f);
            Debug.Log(hit.point);
            hit.collider.gameObject.GetComponent<CellDelegate>().AddPoint(new Vector3d(hit.point));
            count = -1;
            if (count == 0)
            {
                hit.collider.gameObject.GetComponent<CellDelegate>().AddPoint(point);
                count++;
            }
            else if (count == 1)
            {
                hit.collider.gameObject.GetComponent<CellDelegate>().AddPoint(point2);
            }
        }

    }
}
