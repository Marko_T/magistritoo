﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawTriangulation : MonoBehaviour {

    BowyerWatson b;

    [SerializeField]
    int PointCount = 10;

    public GameObject player;

    public int cell = 0;

    private bool _drawTriangulation = false;
    private bool _drawVoronoi = false;
    private bool _drawPoints = false;
    private bool _drawOctree = false;

    public int _drawVoronoiCell = -1;
    private int _drawTetrahedron = -1;

	void Start () {
        Random.InitState(11);

        b = new BowyerWatson(PointCount);
        float time = Time.realtimeSinceStartup;
        b.MakeDiagram();

        /* for(int i = 0; i < b.vertices.Count; i++)
         {
             b.vertices[i].CreateCell();
             b.vertices[i].makeCellIntoObject();
         }*/

  //      b.vertices[8].CreateObject();
        b.vertices[8].CreateCell();
        b.vertices[8].makeCellIntoObject();

     //   GameObject go = GameObject.Instantiate(player);
     //  go.SetActive(true);
     //   go.transform.position = b.vertices[8].loc;
        Debug.Log(Time.realtimeSinceStartup - time);

    }
	
	// Update is called once per frame
	void Update () {
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        float time = Time.realtimeSinceStartup;
       //     b.DoStep();
    //        Debug.Log(Time.realtimeSinceStartup - time);
    //    }


        if (_drawTriangulation)
        {
            b.DrawTriangulation();
        }

        if (_drawVoronoi)
        {
            b.DrawVoronoi();
        }
        // b.vertices[7].CreateCell();
        //    if (_drawOctree) b.DrawOctree();
    }

    public void drawTriangulation()
    {
        _drawTriangulation = !_drawTriangulation;
    }

    public void drawVoronoi()
    {
        _drawVoronoi = !_drawVoronoi;
    }

    public void drawPoints()
    {
        _drawPoints = !_drawPoints;
    }

    public void drawOctree()
    {
        _drawOctree = !_drawOctree;
    }

    public void createObjects()
    {
           for (int i = 0; i < b.vertices.Count; i++)
           {
               //    b.vertices[i].CreateCell();
               //    b.vertices[i].makeCellIntoObject();
           }
           
    //    b.vertices[_drawVoronoiCell].CreateCell();
    //    b.vertices[_drawVoronoiCell].makeCellIntoObject();
    }

    private void OnDrawGizmos()
    {
        if (_drawPoints)
        {
            foreach (Vertex p in b.points)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawSphere(p.loc.vector3, 0.3f);
            }
        }
    }

    private GameObject createCube()
    {
        Vector3[] vertices = new Vector3[8];
        vertices[0] = new Vector3();
        vertices[1] = new Vector3(201, 0, 0);
        vertices[2] = new Vector3(201, 0, 201);
        vertices[3] = new Vector3(0, 0, 201);

        vertices[4] = new Vector3(0,201,0);
        vertices[5] = new Vector3(201, 201, 0);
        vertices[6] = new Vector3(201, 201, 201);
        vertices[7] = new Vector3(0, 201, 201);

        int[] tris = new int[8 * 6];

        tris[0] = 0;
        tris[1] = 1;
        tris[2] = 2;

        tris[3] = 0;
        tris[4] = 2;
        tris[5] = 3;

        tris[6] = 4;
        tris[7] = 6;
        tris[8] = 5;

        tris[9] = 4;
        tris[10] = 7;
        tris[11] = 6;

        tris[12] = 0;
        tris[13] = 3;
        tris[14] = 7;

        tris[15] = 0;
        tris[16] = 7;
        tris[17] = 4;

        tris[18] = 0;
        tris[19] = 5;
        tris[20] = 1;

        tris[21] = 0;
        tris[22] = 4;
        tris[23] = 5;

        tris[24] = 2;
        tris[25] = 1;
        tris[26] = 6;

        tris[27] = 1;
        tris[28] = 5;
        tris[29] = 6;

        tris[30] = 2;
        tris[31] = 7;
        tris[32] = 3;

        tris[33] = 2;
        tris[34] = 6;
        tris[35] = 7;

        Mesh m = new Mesh();

        m.vertices = vertices;
        m.triangles = tris;
        m.RecalculateNormals();

        GameObject o = new GameObject();
        o.AddComponent<MeshFilter>().mesh = m;
        o.AddComponent<MeshCollider>().sharedMesh = m;
        o.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));

        return o;
    }
}
