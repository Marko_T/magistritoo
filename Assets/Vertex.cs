﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Vertex: System.IEquatable<Vertex>
{

    public List<Tetrahedron> neighbors;
    public List<Planed> planes;
    public List<AVoronoiFace> faces;

    public Vector3d loc;

    public double y
    {
        get
        {
            return loc.y;
        }
    }

    public double x
    {
        get
        {
            return loc.x;
        }
    }

    public double z
    {
        get
        {
            return loc.z;
        }
    }

    public List<Vertex> neighbourVertices
    {
        get
        {
            List<Vertex> neighs = new List<Vertex>();
            foreach(Tetrahedron t in neighbors)
            {
                if (t.p1 != this) neighs.Add(t.p1);
                if (t.p2 != this) neighs.Add(t.p2);
                if (t.p3 != this) neighs.Add(t.p3);
                if (t.p4 != this) neighs.Add(t.p4);
            }
            return neighs.Distinct(new VertexComparer()).ToList();
        }
    }

    public List<Vertex> vertices;
    public List<int> indices;
    Vector3d[] normals;

    public BowyerWatson bw;

    public GameObject go;

    private bool disable = true;

	public Vertex() {
        neighbors = new List<Tetrahedron>();
        indices = new List<int>();
        planes = new List<Planed>();
	}

    public Vertex(float x, float y, float z)
    {
        loc = new Vector3d(x, y, z);
        neighbors = new List<Tetrahedron>();
        indices = new List<int>();
        planes = new List<Planed>();
    }

    public Vertex(Vector3d point)
    {
        loc = point;
        neighbors = new List<Tetrahedron>();
        indices = new List<int>();
        planes = new List<Planed>();
    }

    public void getPlanes()
    {
        foreach(Tetrahedron t in neighbors)
        {
            foreach(Face f in t.faces)
            {
                if (this == f.p1)
                {
                    planes.Add(new Planed((f.p2 - f.p1).normalized, (f.p1 + f.p2) / 2, this, f.p2));
                    planes.Add(new Planed((f.p3 - f.p1).normalized, (f.p1 + f.p3) / 2, this, f.p3));
                }
                else if (this == f.p2)
                {
                    planes.Add(new Planed((f.p1 - f.p2).normalized, (f.p1 + f.p2) / 2, this, f.p1));
                    planes.Add(new Planed((f.p3 - f.p2).normalized, (f.p2 + f.p3) / 2, this, f.p3));
                }
                else if (this == f.p3)
                {
                    planes.Add(new Planed((f.p2 - f.p3).normalized, (f.p3 + f.p2) / 2, this, f.p2));
                    planes.Add(new Planed((f.p1 - f.p3).normalized, (f.p1 + f.p3) / 2, this, f.p1));
                }
            }
        }
    }

    public void CreateCell()
    {
        faces = new List<AVoronoiFace>();

        getPlanes();
        planes = planes.Distinct().ToList();

        List<Vector3d> normalizedPoints = new List<Vector3d>();
        neighbors.ForEach(x => normalizedPoints.Add(x.circumceter - loc));
        for(int i = 0; i < planes.Count; i++)
        {
            
            Planed p = planes[i];
            List<Vertex> points = new List<Vertex>();
            Vector3d centerOfMass = Vector3d.zero;
            for(int j = 0; j < neighbors.Count; j++)
            {
             //   Debug.DrawLine(neighbors[j].circumceter.vector3, neighbors[j].circumceter.vector3 + (p.normal * p.GetDistanceToPoint(neighbors[j].circumceter)).vector3, Color.blue, 10f);
                if (Mathd.Abs(p.GetDistanceToPoint(neighbors[j].circumceter)) < 0.003)
                {
                    points.Add(new Vertex(normalizedPoints[j]));
                    centerOfMass += normalizedPoints[j];
                }
            }

        //    Debug.Log(points.Count);
            if (points.Count < 3) continue;
            centerOfMass /= points.Count;
      
            VoronoiFace child = new VoronoiFace(points, p, centerOfMass, loc);
            faces.Add(new InvisibleVoronoiFace(points, p, centerOfMass, loc, child));
        }
    }

    public bool pointInCell(Vector3d point)
    {
        foreach(AVoronoiFace f in faces)
        {
            if (f.p.GetSide(point))
            {
                return false;
            }
        }
        return true;
    }

    // KKOntrolli kas uued punktid on tühja ala sees, kui jah viska minema, kui ei siis kontrolli kas on mingis X kauguses mõnest olemasolevast punktist
    // kui jahs siis pane võrduma

    public void AddPoint(Vector3d point)
    {
        /*  Vertex added = bw.AddPoint(point);
          added.CreateCell();
          added.makeCellIntoObject();*/
        List<Vector3d> newPoints = new List<Vector3d>();
        Vector3d p = point * 10;
        p = new Vector3d((int)p.x / 10f, (int)p.y / 10f, (int)p.z / 10f);
        newPoints.Add(p);

        for(int i = 0; i <200; i++)
        {
            newPoints.Add(GaussianRandom.NextGaussianPoint(p, 20));
        }

        newPoints = newPoints.Distinct(new VectorComparer()).ToList();
   //     newPoints.RemoveAll(x => GameManager.materialized.Skip(1).Any(v => v.pointInCell(x)));
        List<Vertex> newCells = bw.AddPoints(newPoints);

        newCells.RemoveAll(x => { x.CreateCell(); return x.getVolume() >= 400 ||x.getVolume() == 0; });
        int count = GameManager.materialized.Count;
        foreach (Vertex v in newCells)
        {
            for(int i = 0; i < count; i++) {
                CellMerger.MergeCells(GameManager.materialized[i], v);
                break;
            }
            v.makeCellIntoObject();
            v.RemoveObject();
          //  v.DrawFaces();
         //     break;
        }
        DrawCutlines();
     //   RemoveSharedFaces(GameManager.materialized);
    }

    private void DrawCutlines()
    {
        foreach(AVoronoiFace f in faces)
        {
            if (f.child == null) continue;
            foreach(List<Line> lines in f.child.cutLines)
            {
                foreach(Line l in lines)
                {
                    Debug.DrawLine(l.start.vector3, l.end.vector3, Color.red, 10f);
                }
            }
        }
    }

    private void RemoveObject()
    {
        bool face = false;
        foreach(AVoronoiFace f in faces)
        {
            if (f.HasObject())
            {
                face = true;
                break;
            }
        }
        if (!face)
        {
            GameObject.Destroy(go);
            go = null;
            GameManager.materialized.Remove(this);
        }
    }

    private void RemoveSharedFaces(List<Vertex> newPoints)
    {   
        for(int i = 0; i < newPoints.Count; i++)
        {
            for(int j = 0; j < newPoints[i].faces.Count; j++)
            {
                AVoronoiFace f = newPoints[i].faces[j];
                if(f.p.left.go != null && f.p.right.go != null)
                {
                    f.RemoveObject();
                }
            }
            /*
            for(int j = i+1; j < newPoints.Count; j++)
            {
                for(int k = 0; k < newPoints[j].faces.Count; k++)
                {
                    int index = newPoints[i].faces.FindIndex(x => {
                        if (x.child == null || newPoints[j].faces[k].child == null) return false;
                        return x.child.Equals(newPoints[j].faces[k].child);
                        });
                    if(index != -1)
                    {
                        newPoints[i].faces[index].RemoveObject();
                     //   newPoints[i].faces.RemoveAt(index);

                        newPoints[j].faces[k].RemoveObject();
                     //   newPoints[j].faces.RemoveAt(k);
                    }
                }
            }*/
        }
    }

    public GameObject makeCellIntoObject()
    {
        go = new GameObject();
        int i = 0;
        foreach(AVoronoiFace f in faces)
        {
            f.CreateObject(go.transform,i, this);
            i++;
        }

        go.transform.position = loc.vector3;

   //     o.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
   //     Rigidbody rb = go.AddComponent<Rigidbody>();
   //     rb.isKinematic = false;
   //     rb.detectCollisions = true;
        go.tag = "Cell";

        //     go.SetActive(!disable);
        GameManager.materialized.Add(this);
        return go;
    }


    public double getVolume()
    {
        double volume = 0;
        foreach (AVoronoiFace f in faces)
        {
            volume += f.getVolumeValue();
        }
        return volume / 3;
    }

    public void drawTriangulation()
    {
        foreach(Tetrahedron t in neighbors)
        {
            Debug.DrawLine(loc.vector3, t.p1.loc.vector3, Color.red);
            Debug.DrawLine(loc.vector3, t.p1.loc.vector3, Color.red);
            Debug.DrawLine(loc.vector3, t.p2.loc.vector3, Color.red);
            Debug.DrawLine(loc.vector3, t.p3.loc.vector3, Color.red);
            Debug.DrawLine(loc.vector3, t.p4.loc.vector3, Color.red);
        }

        foreach(Planed p in planes)
        {
            Debug.DrawRay(loc.vector3, p.normal.vector3 * 4, Color.cyan);
        }
    }

    public bool Equals(Vertex other)
    {
        return (loc - other.loc).sqrMagnitude < 0.01;

    }

    public override bool Equals(System.Object obj)
    {
        return (loc - ((Vertex)obj).loc).sqrMagnitude < 0.01;
    }

    public void AddIncidentTetrahedron(Tetrahedron t)
    {
        neighbors.Add(t);
    }

    public void RemoveIncidentTetrahedron(Tetrahedron t)
    {
        neighbors.Remove(t);
    }

    public static Vector3d operator -(Vertex v1, Vector3d v2)
    {
        return v1.loc - v2;
    }

    public static Vector3d operator -(Vertex v1, Vertex v2)
    {
        return v1.loc - v2.loc;
    }

    public static Vector3d operator +(Vertex v1, Vector3d v2)
    {
        return v1.loc + v2;
    }

    public static Vector3d operator +(Vertex v1, Vertex v2)
    {
        return v1.loc + v2.loc;
    }

    public void DrawFaces()
    {
        foreach(AVoronoiFace f in faces)
        {
            f.DrawFace(loc);
        }
    }

    public void DrawCell()
    {
        /*  for(int i = 0; i < indices.Count-1; i++)
          {
              Debug.DrawLine(vertices[i], vertices[i + 1], Color.green);
          }*/
        //  Debug.DrawLine(loc, loc + Vector3.up, Color.cyan);
        for (int i = 0; i < neighbors.Count; i++)
        {
            for (int j = 0; j < neighbors.Count; j++)
            {
                if (i != j)
                {
                    if (neighbors[i].shareFace(neighbors[j]) != null)
                    {
                        Debug.DrawLine(neighbors[i].circumceter.vector3, neighbors[j].circumceter.vector3, Color.green);
                    }
                }
            }
        }

    }

    public override string ToString()
    {
        return loc.ToString();
    }

    public string ListToString<T> (List<T> l)
    {
        string s = "[";
        for (int i = 0; i < l.Count; i++)
        {
            s += l[i].ToString() + " ";
        }
        return s + "]";

    }

}
/*
public class Matrix3X3
{
    public float a = 0, b = 0, c = 0, d = 0, e=0, f= 0, g = 0, h= 0, i= 0;

    public Matrix3X3(float a, float b, float c, float d, float e, float f, float g, float h, float i)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
    }

    public static Vector3 operator *(Matrix3X3 m, Vector3 v)
    {
        return new Vector3(m.a * v.x + m.d * v.y + m.g * v.z, m.b * v.x + m.e * v.y + m.h * v.z, m.c * v.x + m.f * v.y + m.i * v.z);
    }

    public static Vector3 operator *(Vector3 v, Matrix3X3 m)
    {
        return new Vector3(v.x * m.a + v.y * m.b + v.z * m.c, v.x * m.d + v.y * m.e + v.z * m.f, v.x * m.g + v.y * m.h + v.z * m.i);
    }

    /*
     * a d g
     * b e h 
     * c f i
     */
/*
    public Matrix3X3 Inverse()
    {
        float aMin = e * i - h * f;
        float dMin = -(b * i - h * c);
        float gMin = b * f - e * c; 

        float bMin = -(d * i - g * f);
        float eMin = a * i - g * c;
        float hMin = -(a * f - d * c);

        float cMin = d * h - g * e;
        float fMin = -(a * h - g * b);
        float iMin = a * e - d * b;

        float inverseDetOrigin = 1/(a * aMin + d * dMin + g * gMin);

        return new Matrix3X3(inverseDetOrigin*aMin, inverseDetOrigin*bMin, inverseDetOrigin*cMin,
            inverseDetOrigin*dMin, inverseDetOrigin*eMin, inverseDetOrigin*fMin,
            inverseDetOrigin*gMin, inverseDetOrigin*hMin, inverseDetOrigin*iMin);
    }
}
*/
