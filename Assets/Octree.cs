﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Octree {

    private int MIN_LIMIT = 0;
    private int MAX_LIMIT = 5;

    private Octree[] children;
    private List<Vertex> vertices;
    private Vector3d loc;
    private Vector3d size;
    private Octree parent;

    private Vector3d bin = new Vector3d(4, 2, 1);

    public bool HasChildren
    {
        get
        {
            return children != null;
        }
    }

    public bool HasVertices
    {
        get
        {
            return vertices.Count > 0;
        }
    }

    public Octree UpperRightFront
    {
        get
        {
            if(children != null)
            {
                return children[0];
            }
            return null;
        }
    }

    public Octree UpperRightBack
    {
        get
        {
            if (children != null)
            {
                return children[1];
            }
            return null;
        }
    }

    public Octree UpperLeftFront
    {
        get
        {
            if (children != null)
            {
                return children[2];
            }
            return null;
        }
    }

    public Octree UpperLeftBack
    {
        get
        {
            if (children != null)
            {
                return children[3];
            }
            return null;
        }
    }

    public Octree LowerRightFront
    {
        get
        {
            if (children != null)
            {
                return children[4];
            }
            return null;
        }
    }

    public Octree LowerRightBack
    {
        get
        {
            if (children != null)
            {
                return children[5];
            }
            return null;
        }
    }

    public Octree LowerLeftFront
    {
        get
        {
            if (children != null)
            {
                return children[6];
            }
            return null;
        }
    }

    public Octree LowerLeftBack
    {
        get
        {
            if (children != null)
            {
                return children[4];
            }
            return null;
        }
    }

    public List<Vertex> Vertices
    {
        get
        {
            return vertices;
        }
    }

    public Octree(Vector3d loc, Vector3d size, Octree parent = null)
    {
        this.loc = loc;
        this.size = size;
        vertices = new List<Vertex>();
        this.parent = parent;
    }

    public void AddPoint(Vertex v)
    {
        if (children == null ||children.Length == 0)
        {
            vertices.Add(v);
            if (vertices.Count > MAX_LIMIT)
            {
                SubDivide();
            }
        }else
        {
            Vector3d dir = v.loc - loc;
            dir.x = (Mathd.Sign(dir.x) + 1) / 2;
            dir.y = (Mathd.Sign(dir.y) + 1) / 2;
            dir.z = (Mathd.Sign(dir.z) + 1) / 2;

            //    int index = (int)(4 * dir.x + 2 * dir.y + dir.z);
            int index = (int)Vector3d.Dot(dir, bin);
            children[index].AddPoint(v);
       /*     for (int i = 0; i < 8; i++)
            {
                if (children[i].Contains(v))
                {
                    children[i].AddPoint(v);
                    break;
                }
            }
            */
        }
    }

    public void RemovePoint(Vertex v)
    {
        if(children == null)
        {
            vertices.Remove(v);
            if(vertices.Count <= MIN_LIMIT)
            {
                parent.Merge();
            }
        }else
        {
            for (int i = 0; i < 8; i++)
            {
                if (children[i].Contains(v))
                {
                    children[i].RemovePoint(v);
                    break;
                }
            }
        }
    }

    private void SubDivide()
    {
        children = new Octree[8];
        Vector3d childSize = size / 2;
    /*    children[0] = new Octree(loc + new Vector3(childSize.x, childSize.y, 0), childSize);
        children[1] = new Octree(loc + childSize, childSize);
        children[2] = new Octree(loc + new Vector3(0, childSize.y, 0), childSize);
        children[3] = new Octree(loc + new Vector3(0, childSize.y, childSize.z), childSize);
        children[4] = new Octree(loc + new Vector3(childSize.x, 0, 0), childSize);
        children[5] = new Octree(loc + new Vector3(childSize.x, 0, childSize.z), childSize);
        children[6] = new Octree(loc, childSize);
        children[7] = new Octree(loc + new Vector3(0, 0, childSize.z), childSize);*/

        children[7] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(1,1,1)), childSize);
        children[6] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(1, 1, -1)), childSize);
        children[5] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(1, -1, 1)), childSize);
        children[4] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(1, -1, -1)), childSize);
        children[3] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(-1, 1, 1)), childSize);
        children[2] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(-1, 1, -1)), childSize);
        children[1] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(-1, -1, 1)), childSize);
        children[0] = new Octree(loc + Vector3d.Scale(childSize, new Vector3d(-1, -1, -1)), childSize);



        for (int i = 0; i < vertices.Count; i++)
        {
            Vector3d dir = vertices[i].loc - loc;
            dir.x = (Mathd.Sign(dir.x) + 1) / 2;
            dir.y = (Mathd.Sign(dir.y) + 1) / 2;
            dir.z = (Mathd.Sign(dir.z) + 1) / 2;

            //     int index = (int)(4 * dir.x + 2 * dir.y + dir.z);
            int index = (int)Vector3d.Dot(dir, bin);
            //   Debug.Log(children[index].Contains(vertices[i]));
            children[index].AddPoint(vertices[i]);
            
     /*       for (int j = 0; j < 8; j++)
            {
                if (children[j].Contains(vertices[i]))
                {
                    children[j].AddPoint(vertices[i]);
                    break;
                }
            }*/
        }

        for (int j = 0; j < 8; j++)
        {
            if (!children[j].HasVertices)
            {
                children = null;
                MAX_LIMIT *= 2;
                break;
            }
        } 
        
        if(children != null)
        {
            vertices.Clear();
        }
    }

    private void Merge()
    {
        for(int i = 0; i < 8; i++)
        {
            vertices.AddRange(children[i].Vertices);
        }
    }

    private bool Contains(Vertex v)
    {
        return v.x >= loc.x-size.x && v.x <= loc.x + size.x &&
               v.y >= loc.y-size.y && v.y <= loc.y + size.y &&
               v.z >= loc.z-size.z && v.z <= loc.z + size.z;
    }

    public List<Vertex> GetNeighboringVertices(Vertex v)
    {
        if (children == null) return vertices;
        else
        {
            Vector3d dir = v.loc - loc;
            dir.x = (Mathd.Sign(dir.x) + 1) / 2;
            dir.y = (Mathd.Sign(dir.y) + 1) / 2;
            dir.z = (Mathd.Sign(dir.z) + 1) / 2;

       //     int index = (int)(4 * dir.x + 2 * dir.y + dir.z);
            int index = (int)Vector3d.Dot(dir, bin);
            return children[index].GetNeighboringVertices(v);
        }
        
        return new List<Vertex>();
    }

    public void AddSuperTetraHedron(Tetrahedron t)
    {
        vertices.Add(t.p1);
        vertices.Add(t.p2);
        vertices.Add(t.p3);
        vertices.Add(t.p4);
    }
/*
    public void DrawOctree()
    {
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, -1, -1)), loc + Vector3.Scale(size, new Vector3(1, -1, -1)), Color.red);
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, -1, -1)), loc + Vector3.Scale(size, new Vector3(-1, 1, -1)), Color.red);
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, -1, -1)), loc + Vector3.Scale(size, new Vector3(-1, -1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(1, -1, -1)), loc + Vector3.Scale(size, new Vector3(1, 1, -1)), Color.red);
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(1, -1, -1)), loc + Vector3.Scale(size, new Vector3(1, -1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(1, 1, -1)), loc + Vector3.Scale(size, new Vector3(1, 1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(1, -1, 1)), loc + Vector3.Scale(size, new Vector3(-1, -1, 1)), Color.red);
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(1, -1, 1)), loc + Vector3.Scale(size, new Vector3(1, 1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, -1, 1)), loc + Vector3.Scale(size, new Vector3(-1, 1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, 1, 1)), loc + Vector3.Scale(size, new Vector3(1, 1, 1)), Color.red);

        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, 1, -1)), loc + Vector3.Scale(size, new Vector3(-1, 1, 1)), Color.red);
        Debug.DrawLine(loc + Vector3.Scale(size, new Vector3(-1, 1, -1)), loc + Vector3.Scale(size, new Vector3(1, 1, -1)), Color.red);


        if (children != null)
        {
            for(int i = 0; i < 8; i++)
            {
                children[i].DrawOctree();
            }
        }
                
    }
    */
}
