﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCJumpState : AState
{
    private float prevY;
    private CharacterController charControl;

    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    Vector3 jumpDir = Vector3.zero;

    public float walkSpeed;
    public float jumpVelocity;

    public CCJumpState(Character character) : base(character)
    {
        this.fallMultiplier = character.fallMultiplier;
        this.lowJumpMultiplier = character.lowJumpMultiplier;
        this.jumpVelocity = character.jumpVelocity;
        this.walkSpeed = character.walkSpeed;
        this.charControl = character.charControl;
    }

    public override void Tick()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 moveDirSide = character.transform.right * horiz * walkSpeed;
        Vector3 moveDirForward = character.transform.forward * vert * walkSpeed;

        prevY = character.position.y;
        jumpDir.y += Physics.gravity.y * Time.deltaTime;
        if (jumpDir.y < 0)
        {
            jumpDir.y += Physics.gravity.y * (fallMultiplier - 1f) * Time.deltaTime;
        }
        else if (jumpDir.y > 0 && !Input.GetButton("Jump"))
        {
            jumpDir.y += Physics.gravity.y * (lowJumpMultiplier - 1f) * Time.deltaTime;
        }
        charControl.Move(jumpDir * Time.deltaTime);
        charControl.Move(moveDirSide * Time.deltaTime);
        charControl.Move(moveDirForward * Time.deltaTime);


        if (Mathf.Abs(prevY - character.position.y) < 0.001)
        {
            character.SetState(character.CCWALKSTATE);
        }
    }

    public override void OnStateEnter()
    {
        character.state = "Jumping";
        jumpDir.y = jumpVelocity;
        charControl.slopeLimit = 90;
    }

    public override void OnStateExit()
    {
        charControl.slopeLimit = 45;
    }
}
