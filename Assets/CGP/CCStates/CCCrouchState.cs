﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCCrouchState : AState
{

    private float walkSpeed;
    private CharacterController charControl;
    public CCCrouchState(Character character) : base(character)
    {
        this.walkSpeed = character.walkSpeed;
        this.charControl = character.charControl;
    }

    public override void Tick()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 moveDirSide = character.transform.right * horiz * walkSpeed;
        Vector3 moveDirForward = character.transform.forward * vert * walkSpeed;

        if (Input.GetButtonUp("Crouch"))
        {
            character.SetState(character.CCWALKSTATE);
        }


        charControl.SimpleMove(moveDirSide);
        charControl.SimpleMove(moveDirForward);
    }

    public override void OnStateEnter()
    {
        character.state = "Crouch";
        character.charControl.height -= 1;
        character.charControl.center -= new Vector3(0, 0.5f, 0);
        character.pl.Crouch();
    }

    public override void OnStateExit()
    {
        character.charControl.height += 1;
        character.charControl.center += new Vector3(0, 0.5f, 0);
        character.pl.UnCrouch();    
    }
}
