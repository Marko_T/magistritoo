﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour {

    public LineRenderer renderer;
    private bool isShowingLaser = false;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(showLaser());
        }       
    }

    IEnumerator showLaser()
    {
        if (isShowingLaser) yield return null;
        isShowingLaser = true;
        renderer.enabled = true;

        yield return new WaitForSeconds(0.05f);

        renderer.enabled = false;
        isShowingLaser = false;
    }
}
