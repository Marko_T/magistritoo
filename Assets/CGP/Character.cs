﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public CharacterController charControl;
    public PlayerLook pl;

    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    public float jumpVelocity;
    public float walkSpeed;

    public CCJumpState CCJUMPSTATE;
    public CCWalkState CCWALKSTATE;
    public CCCrouchState CCCROUCHSTATE;

    public AState currentState;

    public string state;

    private bool godMode = false;
    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }
    // Use this for initialization
    void Start () {
        this.charControl = GetComponent<CharacterController>();
        this.pl = GetComponentInChildren<PlayerLook>();

        CCJUMPSTATE = new CCJumpState(this);
        CCWALKSTATE = new CCWalkState(this);
        CCCROUCHSTATE = new CCCrouchState(this);
        SetState(CCWALKSTATE);
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.G))
        {
            godMode = !godMode;
            charControl.enabled = !godMode;
        }

        if (!godMode)
        {
            currentState.Tick();
        }else
        {
            float horiz = Input.GetAxisRaw("Horizontal");
            float vert = Input.GetAxisRaw("Vertical");

            transform.Translate(transform.GetChild(0).localRotation *  Vector3.forward* walkSpeed * vert *  Time.deltaTime);
        }
	}

    void FixedUpdate()
    {
        currentState.FixedTick();
    }

    public void SetState(AState state)
    {
        if(currentState != null)
        {
            currentState.OnStateExit();
        }

        currentState = state;

        if (currentState != null)
        {
            currentState.OnStateEnter();
        }
    }
}
