﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float speed = 10;

    private Camera cam;

	// Use this for initialization
	void Start () {
        cam = Camera.main;
	}
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;


    private int count = 0;

  //  https://forum.unity.com/threads/looking-with-the-mouse.109250/
    void Update()
    {

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    

        float Dx = 0, Dy = 0;
        if (Input.GetKey(KeyCode.W))
        {
            Dx = 1;
        }else if (Input.GetKey(KeyCode.S))
        {
            Dx = -1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            Dy = -1;
        }else if (Input.GetKey(KeyCode.D))
        {
            Dy = 1;
        }

        transform.position += transform.rotation * 
            new Vector3(Dy*speed*Time.deltaTime, 0, Dx*speed*Time.deltaTime);

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 dir = transform.rotation * Vector3.forward;
            Debug.DrawRay(transform.position, dir * 1000, Color.red);
            CastRay();
        }
	}

    private void CastRay()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.rotation * Vector3.forward, out hit))
        {
            hit.collider.gameObject.GetComponent<Mineable>().OnHit();   
        }
        
    }
}
