﻿using UnityEngine;

//Benchmarkong for the Bowyer-Watson algorithm
public class BenchMark : MonoBehaviour
{
    int i = 0;
    int j = 0;
    float time = 0;

    bool running = false;
    BowyerWatson bw;

    private void Update()
    {
        if (i < 10)
        {
            int count = 16 * (int)Mathf.Pow(2, i);

            if (j < 10)
            {
                bw = new BowyerWatson(count);
                float ctime = Time.realtimeSinceStartup;
                bw.MakeDiagram();
                time += Time.realtimeSinceStartup - ctime;
                // StartCoroutine(RunAlgorithm());
            }
            j++;
            if (j == 11)
            {
                i++;
                j = 0;
                Debug.Log(count + " points took: " + time / 10f + "s on average");
                time = 0;
            }
        }

    }
}
