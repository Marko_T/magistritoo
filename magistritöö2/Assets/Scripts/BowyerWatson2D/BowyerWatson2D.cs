﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BowyerWatson2D
{
    public Triangle super_triangle;
    public List<DoubleVector> points;
    public List<Triangle> triangulation;
    public List<DelaunayVertex2D> vertices;


    private List<Triangle> badTriangles = new List<Triangle>();


    private int i = 0;
    private bool done = false;

    private DoubleQuaternion rotQuat;
    private DoubleQuaternion inverseQuat;

    public BowyerWatson2D(List<DoubleVector> points, Planed plane, DoubleVector center)
    {
        this.points = points;
        CreateRotationMatrix(plane);
        generateSuperTriangle(center);
        triangulation = new List<Triangle>(20);
        vertices = new List<DelaunayVertex2D>();
        triangulation.Add(super_triangle);
    }

    //Creates rotation matrix to rotate all the points on xy-plane
    private void CreateRotationMatrix(Planed p)
    {
        DoubleVector n = p.normal;

        double cosA = DoubleVector.Dot(p.normal, new DoubleVector(0,0,1));
        rotQuat = new DoubleQuaternion(Math.Acos(cosA), DoubleVector.Cross(n, new DoubleVector(0, 0, 1))).normalized;
        inverseQuat = rotQuat.inverse;

    }

    //Generates the initial super triangle to initilaize the process
    private void generateSuperTriangle(DoubleVector center)
    {
        DoubleVector moved = rotQuat * center;
        DelaunayVertex2D p1 = new DelaunayVertex2D(new DoubleVector(0,1,0) * 3000 + moved);
        DelaunayVertex2D p2 = new DelaunayVertex2D(new DoubleQuaternion(2*Math.PI / 3, new DoubleVector(0, 0, 1)) * new DoubleVector(0, 1, 0) * 3000f + moved);
        DelaunayVertex2D p3 = new DelaunayVertex2D(new DoubleQuaternion(-2*Math.PI / 3, new DoubleVector(0, 0, 1)) * new DoubleVector(0, 1, 0) * 3000f + moved);

        super_triangle = new Triangle(p1, p2, p3);
    }

    public void MakeDiagram()
    {
        while (!done)
        {
            doStep();
        }
    }

    private bool doStep()
    {
        if (i < points.Count)
        {
            //ROtate point to xy-plane
            vertices.Add(new DelaunayVertex2D(rotQuat * points[i]));
            badTriangles.Clear();
            //Find all bad triangles
            for (int j = triangulation.Count - 1; j >= 0; j--)
            {
                if (triangulation[j].withinRange(vertices[i]))
                {
                    triangulation[j].bad = true;
                    badTriangles.Add(triangulation[j]);
                    triangulation.RemoveAt(j);
                }
            }

            List<Triangle> newTriangles = new List<Triangle>();
            //Process bad triangles
            for (int j = 0; j < badTriangles.Count; j++)
            {
                Triangle temp = badTriangles[j];
                for (int k = 0; k < 3; k++)
                {
                    Edge e = temp.edges[k];

                    //If edge of a bad triangle is not between two bad triangles, create new triangle with it
                    if (e.left == null || !e.left.bad || e.right == null || !e.right.bad)
                    {
                        Triangle t = new Triangle(vertices[i], e, temp);

                        //Find the neighbours of the new triangle
                        for (int h = 0; h < newTriangles.Count; h++)
                        {
                            Edge f = newTriangles[h].shareEdge(t);
                            if (f != null)
                            {

                                t.replaceEdge(f);
                                f.right = t;
                                
                            }
                        }
                        triangulation.Add(t);
                        newTriangles.Add(t);
                    }
                }
            }
            i++;
            return true;
        }
        else
        {

            //Remove triangles connected with te vertices of the super triangle
            triangulation.RemoveAll(t => t.hasVertex(super_triangle.p1) || t.hasVertex(super_triangle.p2) ||
                    t.hasVertex(super_triangle.p3));

            //Undo the rotation to xy-plane
            foreach (DelaunayVertex2D v in vertices)
            {
                v.loc = inverseQuat * v.loc;
            }
            done = true;
        }
        return false;
    }
}
