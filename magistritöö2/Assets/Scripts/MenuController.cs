﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;
    public void StartSimulation()
    {
        SceneManager.LoadScene("Main");
    }

    public void ExitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

    public void ContinueSimulation()
    {
        crosshair.gameObject.SetActive(!crosshair.gameObject.active);
        menupanel.gameObject.SetActive(!menupanel.gameObject.active);
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        
    }

}
