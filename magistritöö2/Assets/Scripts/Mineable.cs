﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Mineable : MonoBehaviour {

    public VoronoiCell cell;

    public bool destoryed;

    private bool calculationEnded = false;
    private static bool calculateing = false;
	
    public void OnHit()
    {
        if (!calculateing)
        {
            calculateing = true;
            StartCoroutine(calculateMining());
        }
    }

    void calculate()
    {
        cell.Hit();
        calculationEnded = true;
    }

    //Coroutine to calculate the insertion, update nad removal of Voronoi cells
    IEnumerator calculateMining()
    {
        Thread calculateHitThread = new Thread(new ThreadStart(calculate));
        calculateHitThread.Start();

        while (!calculationEnded) yield return false;

        cell.UpdateNeighbours();
        cell.DestoryObject();
        calculationEnded = false;
        calculateing = false;
    }
}
