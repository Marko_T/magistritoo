﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHandler : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;

    private bool paused = false;
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            crosshair.gameObject.SetActive(!crosshair.gameObject.active);
            menupanel.gameObject.SetActive(!menupanel.gameObject.active);

            if (paused)
            {
                Time.timeScale = 0;
            }else
            {
                Time.timeScale = 1;
            }
        }
	}
}
