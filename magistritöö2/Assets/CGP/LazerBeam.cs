﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour {

    public LineRenderer renderer;
    private bool isShowingLaser = false;

    public GameObject hitEffect;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && Time.timeScale > 0)
        {
            StartCoroutine(showLaser());
            CastRay();
        }
    }

    IEnumerator showLaser()
    {
        if (isShowingLaser) yield return null;
        isShowingLaser = true;
        renderer.enabled = true;

        yield return new WaitForSeconds(0.05f);

        renderer.enabled = false;
        isShowingLaser = false;
    }

    private void CastRay()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.parent.parent.position, transform.parent.parent.forward, out hit))
        {
            hit.collider.gameObject.GetComponent<Mineable>().OnHit();

            GameObject effect = Instantiate(hitEffect, hit.point, Quaternion.identity);
            effect.transform.forward = -transform.parent.parent.forward;
            ParticleSystem ps = effect.GetComponent<ParticleSystem>();
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = true;
            Destroy(effect, 1);
        }
    }
}
