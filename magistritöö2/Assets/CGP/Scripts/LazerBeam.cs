﻿using UnityEngine;
using UnityEngine.UI;

public class LazerBeam : MonoBehaviour {

    public LineRenderer renderer;
    public Image loadIndicator;
    private bool isShowingLaser = false;

    public GameObject hitEffect;

    private GameObject prev = null;
    private float waitTimer = 0.5f;
    private float maxWaitTime = 0.5f;

	void Update () {
        if (Time.timeScale > 0)
        {
            if (Input.GetButtonDown("Fire"))
            {
                renderer.enabled = true;
            }

            else if (Input.GetButton("Fire"))
            {
                RaycastHit hit;
                Physics.Raycast(transform.parent.parent.position, transform.parent.parent.forward, out hit);

                if (prev == hit.collider.gameObject)
                {
                    waitTimer -= Time.deltaTime;
                    loadIndicator.fillAmount = 1 - waitTimer / maxWaitTime;
                }
                else
                {
                    waitTimer = maxWaitTime;
                    prev = hit.collider.gameObject;
                }

                if (waitTimer < 0)
                {
                    mine(hit);
                }
            }

            else if (Input.GetButtonUp("Fire"))
            {
                renderer.enabled = false;
                prev = null;
                waitTimer = maxWaitTime;
                loadIndicator.fillAmount = 0;
            }
        }
    }

    private void mine(RaycastHit hit)
    {
        hit.collider.gameObject.GetComponent<Mineable>().OnHit();

        GameObject effect = Instantiate(hitEffect, hit.point, Quaternion.identity);
        effect.transform.forward = -transform.parent.parent.forward;
        ParticleSystem ps = effect.GetComponent<ParticleSystem>();
        ParticleSystem.EmissionModule em = ps.emission;
        em.enabled = true;
        Destroy(effect, 1);
    }
}
