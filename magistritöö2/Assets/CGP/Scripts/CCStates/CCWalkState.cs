﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCWalkState : AState
{

    private float walkSpeed;
    private CharacterController charControl;

    public CCWalkState(Character character) : base(character)
    {
        this.walkSpeed = character.walkSpeed;
        this.charControl = character.charControl;
    }

    public override void Tick()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 moveDirSide = character.transform.right * horiz * walkSpeed;
        Vector3 moveDirForward = character.transform.forward * vert * walkSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            character.SetState(character.CCJUMPSTATE);
        }

        if (Input.GetButtonDown("Crouch"))
        {
            character.SetState(character.CCCROUCHSTATE);
        }

        charControl.SimpleMove(moveDirSide);
        charControl.SimpleMove(moveDirForward);
    }

    public override void OnStateEnter()
    {
        character.state = "Walking";
    }
}
