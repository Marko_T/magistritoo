﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;
    public UIHandler ui;

    private void Start()
    {
        menupanel.GetChild(0).GetComponent<Button>().Select();
    }

    public void StartSimulation()
    {
        SceneManager.LoadScene("Main");
    }

    public void ExitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

    public void ContinueSimulation()
    {
        crosshair.gameObject.SetActive(!crosshair.gameObject.active);
        menupanel.gameObject.SetActive(!menupanel.gameObject.active);
        ui.paused = !ui.paused;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        
    }

}
