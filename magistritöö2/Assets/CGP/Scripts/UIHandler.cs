﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;

    public bool paused = false;
	void Update () {
        if (Input.GetButtonDown("Menu"))
        {
            paused = !paused;
            crosshair.gameObject.SetActive(!crosshair.gameObject.active);
            menupanel.gameObject.SetActive(!menupanel.gameObject.active);

            if (paused)
            {
                Time.timeScale = 0;
                menupanel.GetChild(0).GetComponent<Button>().Select();
            }else
            {
                Time.timeScale = 1;
            }
        }
	}
}
