﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour {

    public float mouseSensitivity;
    public Transform body;

    float xAxsisClamp = 0.0f;

    private bool menuOpen = false;
    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update () {
        if(Time.timeScale > 0) RotateCamera();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
	}

    void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotAmountX = mouseX * mouseSensitivity;
        float rotAmountY = mouseY * mouseSensitivity;

        xAxsisClamp -= rotAmountY;
            
        Vector3 targetRot = transform.rotation.eulerAngles;
        Vector3 bodyRot = body.rotation.eulerAngles;

        targetRot.x -= rotAmountY;
        targetRot.z = 0;
        bodyRot.y += rotAmountX;

        if(xAxsisClamp > 90) {
            xAxsisClamp = targetRot.x = 90;
        }
        else if (xAxsisClamp < -90) {
            xAxsisClamp = -90;
            targetRot.x = 270;
        }

        transform.rotation = Quaternion.Euler(targetRot);
        body.rotation = Quaternion.Euler(bodyRot);
    }

    public void Crouch()
    {
        transform.position = transform.position - Vector3.up;
    }

    public void UnCrouch()
    {
        transform.position = transform.position + Vector3.up;
    }
}
